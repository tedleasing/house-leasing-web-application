package com.uoadi.users.admin.filter;


import com.uoadi.users.common.user.view.UserStateBean;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/admin/*")
public class AdminDirNotAdminDeniedFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        UserStateBean userState = (UserStateBean) session.getAttribute("userStateBean");
        // If the user is logged in but not an admin, redirect him to a
        // permission denied error page
        // If the user is an admin, continue filtering
        if (!userState.isAdmin())
            response.sendRedirect(request.getContextPath() + "/denied.xhtml");
        else
            filterChain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
