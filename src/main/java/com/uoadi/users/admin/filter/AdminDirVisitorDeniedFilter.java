package com.uoadi.users.admin.filter;


import com.uoadi.users.common.user.view.UserStateBean;

import javax.faces.application.FacesMessage;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/admin/*")
public class AdminDirVisitorDeniedFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        UserStateBean userState = (UserStateBean) request.getSession().getAttribute("userStateBean");
        // If the user isn't logged in or tries to access the admin/home page using a URL
        // Redirect him to login
        // If the user is logged in but not an admin, check with the
        // 2nd filter
        if (userState == null || !userState.isLoggedIn()) {
            if (userState != null)
                session.setAttribute("message",  new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Πρέπει πρώτα να συνδεθείτε", ""));
            response.sendRedirect(request.getContextPath() + "/login.xhtml");
        }
        else
            filterChain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
