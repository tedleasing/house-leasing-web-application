package com.uoadi.users.admin.service;


import com.uoadi.users.common.algorithms.dao.AlgorithmsDAOImpl;
import com.uoadi.users.common.algorithms.entities.AlgorithmEntity;
import com.uoadi.users.common.algorithms.helper.AlgorithmHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

@Stateless
public class AlgorithmsService implements Serializable{
    static final Logger LOG = LoggerFactory.getLogger(AlgorithmsService.class);
    private static final long serialVersionUID = 3969434478839635415L;

    private AlgorithmsDAOImpl algorithm;

    public AlgorithmsService() {
        algorithm = new AlgorithmsDAOImpl();
        LOG.info("Got algorithmsDAO");
    }

    /*
    * return a list with all the algorithms
    */
    public List<AlgorithmHelper> getAlgorithms() {
        List<AlgorithmHelper> algorithmList=null;
        algorithmList = algorithm.getAlgorithms();
        LOG.info("Get Algorithms");
        return algorithmList;
    }

    public void insertAlgorithm(AlgorithmEntity algorithmEntity) {
        algorithm.insertAlgorithm(algorithmEntity);
        LOG.info("Insert Algorithms");
    }

    public void updateAlgorithm(AlgorithmEntity algorithmEntity) {
        algorithm.updateAlgorithm(algorithmEntity);
        LOG.info("Update Algorithms");
    }

    public void deleteAlgorithm(AlgorithmEntity algorithmEntity) {
        algorithm.deleteAlgorithm(algorithmEntity);
        LOG.info("Delete Algorithms");
    }

    public List<AlgorithmHelper> getAbleAlgorithm(Byte enable) {
        List<AlgorithmHelper> algorithmEntityList = algorithm.getAbleAlgorithms(enable);
        if(enable == 1)
            LOG.info("Get enable Algorithms");
        else
            LOG.info("Get disable Algorithms");
        return algorithmEntityList;
    }
}
