package com.uoadi.users.admin.view;

import com.uoadi.users.common.user.service.UserService;
import com.uoadi.users.common.user.view.UserStateBean;
import com.uoadi.users.common.user.entities.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class AdminDashboardView implements Serializable {
    static final Logger LOG = LoggerFactory.getLogger(AdminDashboardView.class);
    private static final long serialVersionUID = -194326530946062193L;
    // Properties ---------------------------------------------------------------------------------
    @ManagedProperty("#{userStateBean}")
    private UserStateBean userStateBean;

    @EJB
    private UserService userService;

    // Data ---------------------------------------------------------------------------------------
    private List<UserEntity> unapprovedUsers = null;
    private int unapprovedUsersNumber = 0;

    @PostConstruct
    public void init() {
        unapprovedUsers = userService.getUnapprovedUsers();
        unapprovedUsersNumber = unapprovedUsers != null ? unapprovedUsers.size() : 0;
        LOG.info("Admin [" + userStateBean.getUsername() +"] view unapproved users list");
    }

    public int getUnapprovedUsersNumber() {
        return unapprovedUsersNumber;
    }

    public void setUnapprovedUsersNumber(int unapprovedUsersNumber) {
        this.unapprovedUsersNumber = unapprovedUsersNumber;
    }

    public List<UserEntity> getUnapprovedUsers() {
        return unapprovedUsers;
    }

    public void setUnapprovedUsers(List<UserEntity> unapprovedUsers) {
        this.unapprovedUsers = unapprovedUsers;
    }

    public UserStateBean getUserStateBean() {
        return userStateBean;
    }

    public void setUserStateBean(UserStateBean userStateBean) {
        this.userStateBean = userStateBean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdminDashboardView)) return false;

        AdminDashboardView that = (AdminDashboardView) o;

        if (unapprovedUsersNumber != that.unapprovedUsersNumber) return false;
        if (unapprovedUsers != null ? !unapprovedUsers.equals(that.unapprovedUsers) : that.unapprovedUsers != null)
            return false;
        if (!userService.equals(that.userService)) return false;
        if (userStateBean != null ? !userStateBean.equals(that.userStateBean) : that.userStateBean != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userStateBean != null ? userStateBean.hashCode() : 0;
        result = 31 * result + userService.hashCode();
        result = 31 * result + (unapprovedUsers != null ? unapprovedUsers.hashCode() : 0);
        result = 31 * result + unapprovedUsersNumber;
        return result;
    }
}
