package com.uoadi.users.admin.view;

import com.uoadi.users.common.user.service.UserService;
import com.uoadi.users.common.user.view.UserStateBean;
import com.uoadi.users.common.user.entities.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class AdminUsersView implements Serializable {
    static final Logger LOG = LoggerFactory.getLogger(AdminDashboardView.class);
    private static final long serialVersionUID = -3258243171754243424L;
    // Properties ---------------------------------------------------------------------------------
    @ManagedProperty("#{userStateBean}")
    private UserStateBean userStateBean;

    @EJB
    private UserService userService;

    // Data
    private List<UserEntity> users;
    private UserEntity selectedUser;
    private int usersNumber = 0;


    @PostConstruct
    public void init() {
        users = userService.getAllUsers(userStateBean.getUsername());
        selectedUser = new UserEntity();
        usersNumber = users != null ? users.size() : 0;
        LOG.info("Admin [" + userStateBean.getUsername() +"] view users list");
    }

    // Getters and Setters ------------------------------------------------------------------------


    public int getUsersNumber() {
        return usersNumber;
    }

    public void setUsersNumber(int usersNumber) {
        this.usersNumber = usersNumber;
    }

    public UserEntity getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(UserEntity selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    public UserStateBean getUserStateBean() {
        return userStateBean;
    }

    public void setUserStateBean(UserStateBean userStateBean) {
        this.userStateBean = userStateBean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdminUsersView)) return false;

        AdminUsersView that = (AdminUsersView) o;

        if (usersNumber != that.usersNumber) return false;
        if (selectedUser != null ? !selectedUser.equals(that.selectedUser) : that.selectedUser != null) return false;
        if (!userService.equals(that.userService)) return false;
        if (userStateBean != null ? !userStateBean.equals(that.userStateBean) : that.userStateBean != null)
            return false;
        if (users != null ? !users.equals(that.users) : that.users != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userStateBean != null ? userStateBean.hashCode() : 0;
        result = 31 * result + userService.hashCode();
        result = 31 * result + (users != null ? users.hashCode() : 0);
        result = 31 * result + (selectedUser != null ? selectedUser.hashCode() : 0);
        result = 31 * result + usersNumber;
        return result;
    }
}
