package com.uoadi.users.admin.view;

import com.uoadi.users.admin.service.AlgorithmsService;
import com.uoadi.users.common.algorithms.entities.AlgorithmEntity;
import com.uoadi.users.common.algorithms.helper.AlgorithmHelper;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AlgorithmsView implements Serializable {
    static final Logger LOG = LoggerFactory.getLogger(AdminDashboardView.class);
    private static final long serialVersionUID = -194326530946062193L;

    private List<AlgorithmHelper> algorithms;
    private List<AlgorithmHelper> enableAlgorithms;
    private String title;
    private String description;
    private boolean enable;
    private int enableAlgorithmsNumber;

    @EJB
    private AlgorithmsService algorithmsService;

    @PostConstruct
    public void init() {
        algorithms = algorithmsService.getAlgorithms();
        if(algorithms == null) {
            algorithms = new ArrayList<AlgorithmHelper>();
        }
        enableAlgorithms = algorithmsService.getAbleAlgorithm((byte)1);
        if(enableAlgorithms == null)
        {
            enableAlgorithms = new ArrayList<AlgorithmHelper>();
        }
        LOG.info("Created algorithms list");
    }

    public void onRowEdit(RowEditEvent event) {
        AlgorithmHelper helper = ((AlgorithmHelper) event.getObject());
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Η αλλαγή σας αποθηκεύθηκε επιτυχώς", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        algorithmsService.updateAlgorithm(helper.getAlgorithmEntity());
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", ((AlgorithmHelper) event.getObject()).getAlgorithmEntity().getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void setAlgorithms(List<AlgorithmHelper> algorithms) {
        this.algorithms = algorithms;
    }

    public List<AlgorithmHelper> getAlgorithms() {
        return algorithms;
    }

    public void delete(ActionEvent event) {
        AlgorithmHelper helper;
        for(UIComponent component : event.getComponent().getChildren()){
            if( component instanceof UIParameter){
                UIParameter param = (UIParameter) component;
                if(param.getName().equals("delete")){
                    helper = (AlgorithmHelper) param.getValue();
                    algorithmsService.deleteAlgorithm(helper.getAlgorithmEntity());
                    algorithms.remove(helper);
                    if(helper.isEnable())
                        enableAlgorithms.remove(helper);
                }
            }
        }
    }

    public void saveAlgorithm() {
        AlgorithmEntity algorithmEntity = new AlgorithmEntity();
        if(enable)
            algorithmEntity.setEnabled((byte)1);
        else
            algorithmEntity.setEnabled((byte)0);
        algorithmEntity.setTitle(title);
        algorithmEntity.setDescription(description);
        algorithmsService.insertAlgorithm(algorithmEntity);
        AlgorithmHelper helper = new AlgorithmHelper();
        helper.setAlgorithmEntity(algorithmEntity);
        helper.setEnable(enable);
        algorithms.add(helper);
        if(enable)
            enableAlgorithms.add(helper);
        LOG.info("add algorithm to list");
        enable = false;
        title = "";
        description = "";
    }

    public void setEnableAlgorithms(List<AlgorithmHelper> enableAlgorithms) {
        this.enableAlgorithms = enableAlgorithms;
    }

    public int getEnableAlgorithmsNumber() {
        return enableAlgorithms.size();
    }

    public void setEnableAlgorithmsNumber(int enableAlgorithmsNumber) {
        this.enableAlgorithmsNumber = enableAlgorithmsNumber;
    }

    public List<AlgorithmHelper> getEnableAlgorithms() {
        return algorithmsService.getAbleAlgorithm((byte)1);
    }

    public List<AlgorithmHelper> getDisableAlgorithms() {
        return algorithmsService.getAbleAlgorithm((byte)1);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isEnable() {
        return enable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlgorithmsView that = (AlgorithmsView) o;

        if (enable != that.enable) return false;
        if (enableAlgorithmsNumber != that.enableAlgorithmsNumber) return false;
        if (algorithms != null ? !algorithms.equals(that.algorithms) : that.algorithms != null) return false;
        if (algorithmsService != null ? !algorithmsService.equals(that.algorithmsService) : that.algorithmsService != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (enableAlgorithms != null ? !enableAlgorithms.equals(that.enableAlgorithms) : that.enableAlgorithms != null)
            return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = algorithms != null ? algorithms.hashCode() : 0;
        result = 31 * result + (enableAlgorithms != null ? enableAlgorithms.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (enable ? 1 : 0);
        result = 31 * result + enableAlgorithmsNumber;
        result = 31 * result + (algorithmsService != null ? algorithmsService.hashCode() : 0);
        return result;
    }
}
