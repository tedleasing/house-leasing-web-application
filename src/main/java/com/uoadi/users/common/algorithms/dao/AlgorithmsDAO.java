package com.uoadi.users.common.algorithms.dao;


import com.uoadi.users.common.algorithms.entities.AlgorithmEntity;
import com.uoadi.users.common.algorithms.helper.AlgorithmHelper;

import java.util.List;

public interface AlgorithmsDAO {
    /*
    * return all the algorithms
    */
    public List<AlgorithmHelper> getAlgorithms();

    /*
    * get enable or disable algorithms
    */
    public List<AlgorithmHelper> getAbleAlgorithms(Byte enable);

    public Integer insertAlgorithm(AlgorithmEntity algorithmEntity);

    public Boolean updateAlgorithm(AlgorithmEntity algorithmEntity);

    public void deleteAlgorithm(AlgorithmEntity algorithmEntity);
}
