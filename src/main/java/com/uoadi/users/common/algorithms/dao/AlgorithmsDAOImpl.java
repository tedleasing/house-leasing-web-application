package com.uoadi.users.common.algorithms.dao;


import com.uoadi.hibernate_related.HibernateUtil;
import com.uoadi.users.common.algorithms.entities.AlgorithmEntity;
import com.uoadi.users.common.algorithms.helper.AlgorithmHelper;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class AlgorithmsDAOImpl implements AlgorithmsDAO{
    static final Logger LOG = LoggerFactory.getLogger(AlgorithmsDAOImpl.class);

    @Override
    public List<AlgorithmHelper> getAlgorithms() {
        Session session = null;
        List generic = null;
        Transaction tx = null;
        ArrayList<AlgorithmHelper> algorithmHelperArrayList;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            generic = session.createQuery("from AlgorithmEntity")
                    .list();
        } catch (HibernateException e) {
            LOG.error("getAlgorithms - Hibernate error");
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if(generic != null && generic.size() != 0) {
            algorithmHelperArrayList = new ArrayList<AlgorithmHelper>();
            for(Object algorithmEntity: generic) {
                AlgorithmEntity algorithm = (AlgorithmEntity) algorithmEntity;
                AlgorithmHelper helper = new AlgorithmHelper();
                helper.setAlgorithmEntity(algorithm);
                if(algorithm.getEnabled() == 1)
                    helper.setEnable(true);
                else
                    helper.setEnable(false);
                algorithmHelperArrayList.add(helper);
            }
            return algorithmHelperArrayList;
        }
        return null;
    }

    @Override
    public List<AlgorithmHelper> getAbleAlgorithms(Byte enable) {
            Session session = null;
            List generic = null;
            Transaction tx = null;
            ArrayList<AlgorithmHelper> algorithmHelperArrayList;
            try {
                // Get a session and a transaction
                session = HibernateUtil.getSessionFactory().getCurrentSession();
                tx = session.getTransaction();
                // Define transaction boundaries
                tx.begin();
                generic = session.createQuery("from AlgorithmEntity where enabled=?")
                        .setByte(0, enable)
                        .list();
            } catch (HibernateException e) {
                LOG.error("getAlgorithms - Hibernate error");
                return null;
            } finally {
                if ((session != null) && (session.isOpen()))
                    session.close();
            }
            if(generic != null && generic.size() != 0) {
                algorithmHelperArrayList = new ArrayList<AlgorithmHelper>();
                for(Object algorithmEntity: generic) {
                    AlgorithmEntity algorithm = (AlgorithmEntity) algorithmEntity;
                    AlgorithmHelper helper = new AlgorithmHelper();
                    helper.setAlgorithmEntity(algorithm);
                    if(algorithm.getEnabled() == 1)
                        helper.setEnable(true);
                    else
                        helper.setEnable(false);
                    algorithmHelperArrayList.add(helper);
                }
                return algorithmHelperArrayList;
            }
            return null;
    }

    @Override
    public Integer insertAlgorithm(AlgorithmEntity algorithmEntity) {
        Session session = null;
        List generic = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();

            Criteria crit = session.createCriteria(AlgorithmEntity.class);
            crit.setProjection(Projections.max("idAlgorithm"));
            List teams = crit.list();
            Integer maxId = (Integer) teams.get(0);
            maxId = (maxId==null) ? 1 : maxId+1;
            algorithmEntity.setIdAlgorithm(maxId+1);

            session.save(algorithmEntity);
            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOG.error("insertAlgorithms - Hibernate error");
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return algorithmEntity.getIdAlgorithm();
    }

    @Override
    public Boolean updateAlgorithm(AlgorithmEntity algorithmEntity) {
        Session session = null;
        List generic = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            session.update(algorithmEntity);
            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOG.error("updateAlgorithms - Hibernate error");
            return false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return true;
    }

    @Override
    public void deleteAlgorithm(AlgorithmEntity algorithmEntity) {
        Session session = null;
        List generic = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            session.delete(algorithmEntity);
            tx.commit();
        } catch (HibernateException e) {
            LOG.error("deleteAlgorithms - Hibernate error");
            return;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return;
    }
}
