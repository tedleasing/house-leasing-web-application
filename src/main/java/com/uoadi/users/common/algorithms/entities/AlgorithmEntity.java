package com.uoadi.users.common.algorithms.entities;

import javax.persistence.*;

/**
 *
 */
@Entity
@Table(name = "algorithm", schema = "", catalog = "myhouse")
public class AlgorithmEntity {
    private int idAlgorithm;
    private String title;
    private String description;
    private byte enabled;

    @Id
    @Column(name = "idAlgorithm", nullable = false, insertable = true, updatable = true)
    public int getIdAlgorithm() {
        return idAlgorithm;
    }

    public void setIdAlgorithm(int idAlgorithm) {
        this.idAlgorithm = idAlgorithm;
    }

    @Basic
    @Column(name = "title", nullable = false, insertable = true, updatable = true, length = 45)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "description", nullable = false, insertable = true, updatable = true, length = 45)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "enabled", nullable = false, insertable = true, updatable = true)
    public byte getEnabled() {
        return enabled;
    }

    public void setEnabled(byte enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlgorithmEntity that = (AlgorithmEntity) o;

        if (enabled != that.enabled) return false;
        if (idAlgorithm != that.idAlgorithm) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAlgorithm;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) enabled;
        return result;
    }
}
