package com.uoadi.users.common.algorithms.helper;


import com.uoadi.users.common.algorithms.entities.AlgorithmEntity;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;

@ManagedBean
public class AlgorithmHelper implements Serializable{
    private AlgorithmEntity algorithmEntity;
    private boolean enable;
    private String isEnable;
    private String title;
    private String description;

    @PostConstruct
    public void init() {
        if(algorithmEntity.getEnabled() == 1)
            enable = true;
        else
            enable = false;
    }

    public AlgorithmEntity getAlgorithmEntity() {
        return algorithmEntity;
    }

    public void setAlgorithmEntity(AlgorithmEntity algorithmEntity) {
        this.algorithmEntity = algorithmEntity;
    }

    public String getIsEnable() {
        if(enable) {
            isEnable = "Ενεργός";
            return isEnable;
        }
        isEnable = "Ανενεργός";
        return isEnable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
        if(enable)
            algorithmEntity.setEnabled((byte)1);
        else
            algorithmEntity.setEnabled((byte)0);
    }

    public boolean isEnable() {
        return enable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
