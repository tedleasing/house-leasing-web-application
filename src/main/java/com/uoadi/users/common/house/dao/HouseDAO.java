package com.uoadi.users.common.house.dao;

import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house.entities.info.ImagesEntity;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.offers.helper.SearchHouse;
import com.uoadi.users.offers.helper.SearchResults;

import java.util.List;

public interface HouseDAO {

    /**
     * Updates the house Entity
     * @param house The updated house entity
     *              to persist
     * @return  True on success, false
     *          otherwise
     */
    Boolean updateHouse(HouseEntity house);

	/**
     * Populates a list of the images submitted by
     * the user for the specific house
     * @param houseID The ID of the house whose images,
     *                are to be retrieved
     * @return The list of Images Entities of the house
     */
    List<ImagesEntity> getAllImages(Integer houseID);

    /**
     * Deletes the image from the DB
     * @param img The image to be deleted
     * @return True on success, otherwise false
     */
    boolean deleteImage(ImagesEntity img);

    /**
     * @return all the houses with their attributes (value, user) depending of the user's role
     */
	List<SearchResults> searchHouse(SearchHouse searchHouse, Boolean isBuyer);

    /**
     * Persists an image at the DB depending
     * by inserting it or updating (see below)
     * @param img The image to be inserted
     * @param isNew Flag that depicts if we need to
     *              save or update an ImageEntity
     * @return True on success, otherwise false
     */
    boolean persistImage(ImagesEntity img, boolean isNew);

    /**
     * @return  the house with the specific id which is for sale or rent depending the value of isBuyer
     */
    SearchResults searchHouseById(int idHouse, Boolean isBuyer);

    /**
     * Persist message from client for
     * house
     * @param msg The message entity to be
     *            persisted
     * @param senderID The userID of the sender
     *          (used for the retrieval of their email)
     * @return True on success, otherwise false
     */
    boolean persistMessage(MessagesEntity msg, int senderID);

    /**
     * Updates already created message to the DB
     * @param msg The message entity to be updated
     * @return True on success, false otherwise
     */
    boolean updateMessage(MessagesEntity msg);
}
