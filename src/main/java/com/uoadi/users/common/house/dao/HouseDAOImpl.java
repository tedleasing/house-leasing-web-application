package com.uoadi.users.common.house.dao;

import com.uoadi.hibernate_related.HibernateUtil;
import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house.entities.info.ImagesEntity;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasLessorEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasSalesmanEntity;
import com.uoadi.users.common.house_transaction.entities.ValueEntity;
import com.uoadi.users.common.user.entities.UserEntity;
import com.uoadi.users.offers.helper.SearchHouse;
import com.uoadi.users.offers.helper.SearchResults;
import org.hibernate.*;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class HouseDAOImpl implements HouseDAO {
    static final Logger LOG = LoggerFactory.getLogger(HouseDAOImpl.class);


    public static HouseDAOImpl getHouseDAOImpl() {
        return new HouseDAOImpl();
    }

    @Override
    public Boolean updateHouse(HouseEntity house) {
        Session session = null;
        Transaction tx = null;
        boolean wasSuccessful = true;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            // Persist address
            session.update(house.getLocationByIdLocation().getAddressByIdAddress());
            // Persist location
            session.update(house.getLocationByIdLocation());
            // Persist house
            session.update(house);
            tx.commit();
        } catch (HibernateException e) {
            if(tx != null)
                tx.rollback();
            LOG.error("updateHouse - Hibernate error: " + e.getMessage());
            wasSuccessful = false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return wasSuccessful;
    }

    @Override
    public List<ImagesEntity> getAllImages(Integer houseID) {
        List genericImages = null;
        ArrayList<ImagesEntity> images = null;
        Session session = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            genericImages = session.createQuery("from ImagesEntity as i where i.houseByIdHouse.idHouse = ?")
                    .setInteger(0, houseID).list();
        } catch (HibernateException e) {
            LOG.error("getAllImages - Hibernate error: " + e.getLocalizedMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if ((genericImages != null) && (genericImages.size() > 0)) {
            images = new ArrayList<ImagesEntity>();
            for(Object genImg: genericImages)
                images.add((ImagesEntity)genImg);
        }
        return images;
    }

    @Override
    public boolean deleteImage(ImagesEntity img) {
        Session session = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            session.delete(img);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            LOG.error("deleteImage - Hibernate error: " + e.getLocalizedMessage());
            return false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return true;
    }

    @Override
    public boolean persistImage(ImagesEntity img, boolean isNew) {
        Session session = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            if (isNew) {
                // imageID handling
                Criteria criteria = session.createCriteria(ImagesEntity.class);
                criteria.setProjection(Projections.max("idImage"));
                List imagesIDs = criteria.list();
                Integer maxId = (Integer) imagesIDs.get(0);
                maxId = (maxId == null) ? 1 : maxId + 1;
                img.setIdImage(maxId);

                session.save(img);
            }
            else
                session.update(img);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            LOG.error("deleteImage - Hibernate error: " + e.getMessage());
            return false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return true;
    }

    @Override
    public List<SearchResults> searchHouse(SearchHouse searchHouse, Boolean isBuyer) {
        Session session = null;
        Transaction tx = null;
        List<SearchResults> result = new ArrayList<SearchResults>();
        List res=null;
        Disjunction disjunction;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            Criteria criteria = null;
            if(isBuyer) {
                criteria = session.createCriteria(HouseHasSalesmanEntity.class)
                        .createAlias("salesmanByIdUser", "S");
            }
            else {
                criteria = session.createCriteria(HouseHasLessorEntity.class)
                        .createAlias("lessorByIdUser", "S");
            }
            criteria
                    .createAlias("houseByIdHouse", "H")
                    .createAlias("valueByIdValue", "V")
                    .createAlias("S.userByIdUser", "U")
                    .createAlias("H.locationByIdLocation", "L")
                    .createAlias("L.addressByIdAddress", "A");
//                    .createAlias("H.imagesByIdHouse", "I");
            criteria.setFetchMode("H.locationByIdLocation",FetchMode.JOIN);
            criteria.setFetchMode("L.addressByIdAddress", FetchMode.JOIN);
            if(isBuyer) {
                criteria.add(Restrictions.eq("U.isSalesman",(byte)1));
            }
            else {
                criteria.add(Restrictions.eq("U.isLeessor",(byte)1));
            }
            if(searchHouse.getFromPrice() != null) {
                criteria.add(Restrictions.ge("V.price",searchHouse.getFromPrice()));
            }
            if(searchHouse.getToPrice() != null) {
                criteria.add(Restrictions.le("V.price", searchHouse.getToPrice()));
            }
            if(searchHouse.getNegotiableABoolean() != null && searchHouse.getNegotiableABoolean()) {
                criteria.add(Restrictions.eq("V.negotiable",searchHouse.getNegotiable()));
            }
            if(searchHouse.getFromSq() != null) {
                criteria.add(Restrictions.ge("H.sq", searchHouse.getFromSq()));
            }
            if(searchHouse.getToSq() != null) {
                criteria.add(Restrictions.le("H.sq", searchHouse.getToSq()));
            }
            if(searchHouse.getFormDateBuilt() != null) {
                criteria.add(Restrictions.ge("H.dateBuilt", searchHouse.getFormDateBuilt()));
            }
            if(searchHouse.getToDateBuilt() != null) {
                criteria.add(Restrictions.le("H.dateBuilt", searchHouse.getToDateBuilt()));
            }
            if(searchHouse.getFromDateRenovation() != null) {
                criteria.add(Restrictions.ge("H.renovationDate", searchHouse.getFromDateRenovation()));
            }
            if(searchHouse.getToDateRenovation() != null) {
                criteria.add(Restrictions.le("H.renovationDate", searchHouse.getToDateRenovation()));
            }
            if(searchHouse.getFromPublicExpenditure() != null) {
                criteria.add(Restrictions.ge("H.publicExpenditure", searchHouse.getFromPublicExpenditure()));
            }
            if(searchHouse.getToPublicExpenditure() != null) {
                criteria.add(Restrictions.le("H.publicExpenditure", searchHouse.getToPublicExpenditure()));
            }
            if(!searchHouse.getHeating().equals("")) {
                criteria.add(Restrictions.like("H.heating", searchHouse.getHeating()));
            }
            if(searchHouse.getApartment() != null) {
                criteria.add(Restrictions.eq("H.apartment",searchHouse.getApartment()));
            }
            if(searchHouse.getFloor() != null) {
                criteria.add(Restrictions.eq("H.floor", searchHouse.getFloor()));
            }
            if(searchHouse.getLocationEntity().getLongitude() != null) {
                criteria.add(Restrictions.eq("L.longitude", searchHouse.getLocationEntity().getLongitude()));
            }
            if(searchHouse.getLocationEntity().getLatitude() != null) {
                criteria.add(Restrictions.eq("L.latitude",searchHouse.getLocationEntity().getLatitude()));
            }
            if(!searchHouse.getLocationEntity().getState().equals("")) {
                criteria.add(Restrictions.like("L.state",searchHouse.getLocationEntity().getState()));
            }
            if(!searchHouse.getLocationEntity().getCity().equals("")) {
                criteria.add(Restrictions.like("L.city",searchHouse.getLocationEntity().getCity()));
            }
            if(!searchHouse.getAddressEntity().getRoadName().equals("")) {
                criteria.add(Restrictions.like("A.roadName",searchHouse.getAddressEntity().getRoadName()));
            }
            if(!searchHouse.getAddressEntity().getHouseNumber().equals("")) {
                criteria.add(Restrictions.eq("A.houseNumber",searchHouse.getAddressEntity().getHouseNumber()));
            }
            res = criteria.list();
            if(res != null && res.size() > 0) {
                for(Object search : res) {
                    SearchResults searchResults = new SearchResults();
                    if(isBuyer) {
                        HouseHasSalesmanEntity houseHasEntity = (HouseHasSalesmanEntity) search;
                        searchResults.setValueEntity(houseHasEntity.getValueByIdValue());
                        searchResults.setHouseEntity(houseHasEntity.getHouseByIdHouse());
                        searchResults.setUserEntity(houseHasEntity.getSalesmanByIdUser().getUserByIdUser());
                    }
                    else {
                        HouseHasLessorEntity houseHasEntity = (HouseHasLessorEntity) search;
                        searchResults.setValueEntity(houseHasEntity.getValueByIdValue());
                        searchResults.setHouseEntity(houseHasEntity.getHouseByIdHouse());
                        searchResults.setUserEntity(houseHasEntity.getLessorByIdUser().getUserByIdUser());
                    }
                    result.add(searchResults);
                }
            }
        } catch (HibernateException e) {
            LOG.error("searchHouse - Hibernate error" + e.getLocalizedMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
//        LOG.debug("deleteHouse: delete house with id: [ " + houseEntity.getIdHouse() + " ]");
        return result;
    }

    @Override
    public SearchResults searchHouseById(int idHouse, Boolean isBuyer) {
        Session session = null;
        Transaction tx = null;
        List Generic = null;
        SearchResults searchResults = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            if(isBuyer)
                Generic = session.createQuery("select H, V, U from HouseHasSalesmanEntity as T, HouseEntity as H, ValueEntity as V, " +
                        "UserEntity as U where T.idHouse = ? and T.idHouse = H.idHouse and T.idUser = U.idUser and T.idValue = V.idValue")
                        .setInteger(0, idHouse)
                        .list();
            else
                Generic = session.createQuery("select H, V, U from HouseHasLessorEntity as T, HouseEntity as H, ValueEntity as V, " +
                        "UserEntity as U where T.idHouse = ? and T.idHouse = H.idHouse and T.idUser = U.idUser and T.idValue = V.idValue")
                        .setInteger(0, idHouse)
                        .list();
            if (Generic != null) {
                searchResults = new SearchResults();
                for (Object search: Generic) {
                    Object[] hhsArray = (Object[]) search;
                    searchResults.setHouseEntity((HouseEntity) hhsArray[0]);
                    searchResults.setValueEntity((ValueEntity) hhsArray[1]);
                    searchResults.setUserEntity((UserEntity) hhsArray[2]);
                    // Initialize house related classes
                    // Location and Address
                    Hibernate.initialize(searchResults.getHouseEntity().getLocationByIdLocation());
                    Hibernate.initialize(searchResults.getHouseEntity().getLocationByIdLocation().getAddressByIdAddress());
                    // Images
                    Hibernate.initialize(searchResults.getHouseEntity().getImagesByIdHouse());
                    // Initialize owner communication and name
                    Hibernate.initialize(searchResults.getUserEntity().getNameByIdName());
                    Hibernate.initialize(searchResults.getUserEntity().getCommunicationByIdCommunication());
                }
            }
        } catch (HibernateException e) {
            LOG.error("searchHouseById - Hibernate error");
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }

        return searchResults;
    }

    @Override
    public boolean persistMessage(MessagesEntity msg, int senderID) {
        Session session = null;
        Transaction tx = null;
        UserEntity sender = null;
        List result = null;
        boolean outcome = true;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            // Get senders email
            result = session.createQuery("select U from UserEntity as U " +
                    "where U.idUser = ?")
                    .setInteger(0, senderID).list();
            if (result.size() == 1) {
                sender = (UserEntity) result.get(0);
                msg.setEmail(sender.getCommunicationByIdCommunication().geteMail());
                // messageID handling
                Criteria criteria = session.createCriteria(MessagesEntity.class);
                criteria.setProjection(Projections.max("idMessage"));
                List messagesID = criteria.list();
                Integer maxId = (Integer) messagesID.get(0);
                maxId = (maxId == null) ? 1 : maxId + 1;
                msg.setIdMessage(maxId);

                session.save(msg);
                tx.commit();
            }
            else
                outcome = false;
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            LOG.error("persistMessage - Hibernate error: " + e.getMessage());
            outcome = false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return outcome;
    }


    @Override
    public boolean updateMessage(MessagesEntity msg) {
        Session session = null;
        Transaction tx = null;
        boolean wasSuccessful = true;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            // Update message
            session.update(msg);
            tx.commit();
        } catch (HibernateException e) {
            if(tx != null)
                tx.rollback();
            LOG.error("updateMessage - Hibernate error: " + e.getMessage());
            wasSuccessful = false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return wasSuccessful;
    }
}
