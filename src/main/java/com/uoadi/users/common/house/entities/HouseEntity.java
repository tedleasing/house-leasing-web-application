package com.uoadi.users.common.house.entities;

import com.uoadi.users.common.house.entities.info.ImagesEntity;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "house", schema = "", catalog = "myhouse")
public class HouseEntity {
    private int idHouse;
    private Integer dateBuilt;
    private Integer renovationDate;
    private Integer publicExpenditure;
    private String heating;
    private Integer sq;
    private byte apartment;
    private Integer floor;
    private String title;
    private LocationEntity locationByIdLocation;
    private Collection<ImagesEntity> imagesByIdHouse;
    private Collection<MessagesEntity> messagesByIdHouse;

    @Id
    @Column(name = "idHouse", nullable = false, insertable = true, updatable = true)
    public int getIdHouse() {
        return idHouse;
    }

    public void setIdHouse(int idHouse) {
        this.idHouse = idHouse;
    }

    @Basic
    @Column(name = "dateBuilt", nullable = true, insertable = true, updatable = true)
    public Integer getDateBuilt() {
        return dateBuilt;
    }

    public void setDateBuilt(Integer dateBuilt) {
        this.dateBuilt = dateBuilt;
    }

    @Basic
    @Column(name = "renovationDate", nullable = true, insertable = true, updatable = true)
    public Integer getRenovationDate() {
        return renovationDate;
    }

    public void setRenovationDate(Integer renovationDate) {
        this.renovationDate = renovationDate;
    }

    @Basic
    @Column(name = "publicExpenditure", nullable = true, insertable = true, updatable = true)
    public Integer getPublicExpenditure() {
        return publicExpenditure;
    }

    public void setPublicExpenditure(Integer publicExpenditure) {
        this.publicExpenditure = publicExpenditure;
    }

    @Basic
    @Column(name = "heating", nullable = true, insertable = true, updatable = true, length = 45)
    public String getHeating() {
        return heating;
    }

    public void setHeating(String heating) {
        this.heating = heating;
    }

    @Basic
    @Column(name = "sq", nullable = false, insertable = true, updatable = true)
    public Integer getSq() {
        return sq;
    }

    public void setSq(Integer sq) {
        this.sq = sq;
    }

    @Basic
    @Column(name = "apartment", nullable = false, insertable = true, updatable = true)
    public byte getApartment() {
        return apartment;
    }

    public void setApartment(byte apartment) {
        this.apartment = apartment;
    }

    @Basic
    @Column(name = "floor", nullable = false, insertable = true, updatable = true)
    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    @Basic
    @Column(name = "title", nullable = true, insertable = true, updatable = true, length = 15)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HouseEntity that = (HouseEntity) o;

        if (apartment != that.apartment) return false;
        if (idHouse != that.idHouse) return false;
        if (dateBuilt != null ? !dateBuilt.equals(that.dateBuilt) : that.dateBuilt != null) return false;
        if (floor != null ? !floor.equals(that.floor) : that.floor != null) return false;
        if (heating != null ? !heating.equals(that.heating) : that.heating != null) return false;
        if (imagesByIdHouse != null ? !imagesByIdHouse.equals(that.imagesByIdHouse) : that.imagesByIdHouse != null)
            return false;
        if (locationByIdLocation != null ? !locationByIdLocation.equals(that.locationByIdLocation) : that.locationByIdLocation != null)
            return false;
        if (messagesByIdHouse != null ? !messagesByIdHouse.equals(that.messagesByIdHouse) : that.messagesByIdHouse != null)
            return false;
        if (publicExpenditure != null ? !publicExpenditure.equals(that.publicExpenditure) : that.publicExpenditure != null)
            return false;
        if (renovationDate != null ? !renovationDate.equals(that.renovationDate) : that.renovationDate != null)
            return false;
        if (sq != null ? !sq.equals(that.sq) : that.sq != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idHouse;
        result = 31 * result + (dateBuilt != null ? dateBuilt.hashCode() : 0);
        result = 31 * result + (renovationDate != null ? renovationDate.hashCode() : 0);
        result = 31 * result + (publicExpenditure != null ? publicExpenditure.hashCode() : 0);
        result = 31 * result + (heating != null ? heating.hashCode() : 0);
        result = 31 * result + (sq != null ? sq.hashCode() : 0);
        result = 31 * result + (int) apartment;
        result = 31 * result + (floor != null ? floor.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (locationByIdLocation != null ? locationByIdLocation.hashCode() : 0);
        result = 31 * result + (imagesByIdHouse != null ? imagesByIdHouse.hashCode() : 0);
        result = 31 * result + (messagesByIdHouse != null ? messagesByIdHouse.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "idLocation", referencedColumnName = "idLocation", nullable = false)
    public LocationEntity getLocationByIdLocation() {
        return locationByIdLocation;
    }

    public void setLocationByIdLocation(LocationEntity locationByIdLocation) {
        this.locationByIdLocation = locationByIdLocation;
    }

    @OneToMany(mappedBy = "houseByIdHouse")
    public Collection<ImagesEntity> getImagesByIdHouse() {
        return imagesByIdHouse;
    }

    public void setImagesByIdHouse(Collection<ImagesEntity> imagesByIdHouse) {
        this.imagesByIdHouse = imagesByIdHouse;
    }

    @OneToMany(mappedBy = "houseByIdHouse")
    public Collection<MessagesEntity> getMessagesByIdHouse() {
        return messagesByIdHouse;
    }

    public void setMessagesByIdHouse(Collection<MessagesEntity> messagesByIdHouse) {
        this.messagesByIdHouse = messagesByIdHouse;
    }
}
