package com.uoadi.users.common.house.entities.info;

import com.uoadi.users.common.house.entities.HouseEntity;

import javax.persistence.*;

/**
 *
 */
@Entity
@Table(name = "images", schema = "", catalog = "myhouse")
public class ImagesEntity {
    private int idImage;
    private String url;
    private String description;
    private HouseEntity houseByIdHouse;

    public boolean isempty() {
        if(url == null && description == null && houseByIdHouse == null)
            return  true;
        return false;
    }

    @Id
    @Column(name = "idImage", nullable = false, insertable = true, updatable = true)
    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    @Basic
    @Column(name = "url", nullable = true, insertable = true, updatable = true, length = 45)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImagesEntity that = (ImagesEntity) o;

        if (idImage != that.idImage) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idImage;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "idHouse", referencedColumnName = "idHouse", nullable = false)
    public HouseEntity getHouseByIdHouse() {
        return houseByIdHouse;
    }

    public void setHouseByIdHouse(HouseEntity houseByIdHouse) {
        this.houseByIdHouse = houseByIdHouse;
    }
}
