package com.uoadi.users.common.house.entities.info;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 *
 */
@Entity
@Table(name = "messages", schema = "", catalog = "myhouse")
public class MessagesEntity {
    private int idMessage;
    private int idHouse;
    private String message;
    private String email;
    private Timestamp time;
    private String title;
    private byte isRead;

    @Id
    @Column(name = "idMessage", nullable = false, insertable = true, updatable = true)
    public int getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(int idMessage) {
        this.idMessage = idMessage;
    }

    @Basic
    @Column(name = "idHouse", nullable = false, insertable = true, updatable = true)
    public int getIdHouse() {
        return idHouse;
    }

    public void setIdHouse(int idHouse) {
        this.idHouse = idHouse;
    }

    @Basic
    @Column(name = "message", nullable = true, insertable = true, updatable = true, length = 200)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "email", nullable = true, insertable = true, updatable = true, length = 45)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "time", nullable = true, insertable = true, updatable = true)
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "title", nullable = false, insertable = true, updatable = true, length = 45)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessagesEntity that = (MessagesEntity) o;

        if (idHouse != that.idHouse) return false;
        if (idMessage != that.idMessage) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idMessage;
        result = 31 * result + idHouse;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "isRead", nullable = false, insertable = true, updatable = true)
    public byte getIsRead() {
        return isRead;
    }

    public void setIsRead(byte isRead) {
        this.isRead = isRead;
    }
}
