package com.uoadi.users.common.house.entities.location;

import javax.persistence.*;

/**
 *
 */
@Entity
@Table(name = "address", schema = "", catalog = "myhouse")
public class AddressEntity {
    private int idAddress;
    private String roadName;
    private String houseNumber;

    @Id
    @Column(name = "idAddress", nullable = false, insertable = true, updatable = true)
    public int getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(int idAddress) {
        this.idAddress = idAddress;
    }

    @Basic
    @Column(name = "roadName", nullable = false, insertable = true, updatable = true, length = 45)
    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    @Basic
    @Column(name = "houseNumber", nullable = false, insertable = true, updatable = true, length = 4)
    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressEntity that = (AddressEntity) o;

        if (idAddress != that.idAddress) return false;
        if (houseNumber != null ? !houseNumber.equals(that.houseNumber) : that.houseNumber != null) return false;
        if (roadName != null ? !roadName.equals(that.roadName) : that.roadName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAddress;
        result = 31 * result + (roadName != null ? roadName.hashCode() : 0);
        result = 31 * result + (houseNumber != null ? houseNumber.hashCode() : 0);
        return result;
    }
}
