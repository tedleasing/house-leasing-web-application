package com.uoadi.users.common.house_transaction.dao;

import com.uoadi.users.offers.helper.HouseValuePack;
import com.uoadi.users.offers.helper.InsertHouse;
import com.uoadi.users.offers.helper.Latlng;
import com.uoadi.users.common.house_transaction.entities.ValueEntity;

import java.util.List;

public interface HouseTransactionsDAO {

    /**
     * @return
     */
    public Integer insertHouse(InsertHouse house);


    /**
     * Deletes the house from the DB, and ensures that all its dependencies in the DB
     * are also deleted.
     * These include:
     *      1) Address
     *      2) Location
     *      3) Messages
     *      4) Images
     *      5) Value
     *      6) Entry from HouseHasSalesman or HouseHasLessor
     *
     * @param houseValuePack The package containing the house and its value
     *                       to be deleted
     * @param isFromSalesman A flag that indicates from where we should
     *                       delete the entry (HouseHasSalesman/Lessor)
     * @return True on success, false on failure
     */
    public boolean deleteHouse(HouseValuePack houseValuePack, boolean isFromSalesman);



    /**
     * Finds all the houses submitted by the user
     * @param userID The user whose houses we need to find
     * @param isSalesman If that's true, then we are checking the HouseHasSalesman table,
     *                   otherwise the HouseHasLessee table
     * @return A list of HouseValuePack
     *         (sth like a tuple containing the HouseID and ValueID)
     */
    List<HouseValuePack> housesOfferedFromUser(Integer userID, boolean isSalesman);

    /**
     * Persists the ValueEntity that is passed to it
     * @param value The ValueEntity to be updated
     * @return True if successful, otherwise false
     */
    boolean updateHouseValue(ValueEntity value);

    public List<Latlng> getCoordinatesHousesUser(Integer userID, boolean isSalesman);
}
