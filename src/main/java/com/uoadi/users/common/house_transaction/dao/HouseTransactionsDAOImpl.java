package com.uoadi.users.common.house_transaction.dao;

import com.uoadi.hibernate_related.HibernateUtil;
import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house.entities.location.AddressEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasLessorEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasSalesmanEntity;
import com.uoadi.users.offers.helper.HouseValuePack;
import com.uoadi.users.offers.helper.InsertHouse;
import com.uoadi.users.offers.helper.Latlng;
import com.uoadi.users.common.house_transaction.entities.ValueEntity;
import org.hibernate.*;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class HouseTransactionsDAOImpl implements HouseTransactionsDAO {
    static final Logger LOG = LoggerFactory.getLogger(HouseTransactionsDAOImpl.class);

    @Override
    public Integer insertHouse(InsertHouse house) {
        HouseEntity houseEntity = house.getHouseEntity();
        AddressEntity addressEntity = house.getAddressEntity();
        LocationEntity locationEntity = house.getLocationEntity();
        ValueEntity valueEntity = house.getValueEntity();
        Session session = null;
        Transaction tx = null;
        Integer maxId;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();

            Criteria crit = session.createCriteria(AddressEntity.class);
            crit.setProjection(Projections.max("idAddress"));
            List teams = crit.list();
            maxId = (Integer) teams.get(0);
            maxId = (maxId==null) ? 1 : maxId+1;
            addressEntity.setIdAddress(maxId);
            session.save(addressEntity);

            crit = session.createCriteria(LocationEntity.class);
            crit.setProjection(Projections.max("idLocation"));
            teams = crit.list();
            maxId = (Integer) teams.get(0);
            maxId = (maxId==null) ? 1 : maxId+1;
            locationEntity.setIdLocation(maxId);
            locationEntity.setAddressByIdAddress(addressEntity);
            session.save(locationEntity);

            crit = session.createCriteria(ValueEntity.class);
            crit.setProjection(Projections.max("idValue"));
            teams = crit.list();
            maxId = (Integer) teams.get(0);
            maxId = (maxId==null) ? 1 : maxId+1;
            valueEntity.setIdValue(maxId);
            session.save(valueEntity);

            crit = session.createCriteria(HouseEntity.class);
            crit.setProjection(Projections.max("idHouse"));
            teams = crit.list();
            maxId = (Integer) teams.get(0);
            maxId = (maxId==null) ? 1 : maxId+1;
            houseEntity.setIdHouse(maxId);
            houseEntity.setLocationByIdLocation(locationEntity);
            session.save(houseEntity);

            if(house.getHouseHasLessorEntity() != null) {
                HouseHasLessorEntity houseHasLessorEntity = house.getHouseHasLessorEntity();
                houseHasLessorEntity.setIdHouse(houseEntity.getIdHouse());
                houseHasLessorEntity.setHouseByIdHouse(houseEntity);
                houseHasLessorEntity.setIdValue(valueEntity.getIdValue());
                houseHasLessorEntity.setValueByIdValue(valueEntity);
                session.save(houseHasLessorEntity);
            }

            if(house.getHouseHasSalesmanEntity() != null) {
                HouseHasSalesmanEntity houseHasSalesmanEntity = house.getHouseHasSalesmanEntity();
                houseHasSalesmanEntity.setIdHouse(houseEntity.getIdHouse());
                houseHasSalesmanEntity.setHouseByIdHouse(houseEntity);
                houseHasSalesmanEntity.setIdValue(valueEntity.getIdValue());
                houseHasSalesmanEntity.setValueByIdValue(valueEntity);
                session.save(houseHasSalesmanEntity);
            }

            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOG.error("insertHouse - Hibernate error " + e.getMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        LOG.debug("insertHouse: Insert house with id: [ " + houseEntity.getIdHouse() + " ]");
        return houseEntity.getIdHouse();
    }

    @Override
    public boolean deleteHouse(HouseValuePack houseValuePack, boolean isFromSalesman) {
        Session session = null;
        Transaction tx = null;
        boolean wasSuccessful = true;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();

            // Merge previously viewed house to the current session
            Object genericHouseToBeDeleted = session.merge(houseValuePack.getHouse());
            HouseEntity mergedHouse = (HouseEntity) genericHouseToBeDeleted;

            // Handle entry of the transactions tables
            if (isFromSalesman) {
                List hhsGeneric = null;
                hhsGeneric = session.createQuery("select T from HouseHasSalesmanEntity as T " +
                        "where T.idHouse = ?")
                        .setInteger(0, mergedHouse.getIdHouse()).list();
                if (hhsGeneric.size() > 0) {
                    HouseHasSalesmanEntity hhs = (HouseHasSalesmanEntity) hhsGeneric.get(0);
                    session.delete(hhs);
                }
            }
            else {
                List hhlGeneric = null;
                hhlGeneric = session.createQuery("select T from HouseHasLessorEntity as T " +
                        "where T.idHouse = ?")
                        .setInteger(0, mergedHouse.getIdHouse()).list();
                if (hhlGeneric.size() > 0) {
                    HouseHasLessorEntity hhs = (HouseHasLessorEntity) hhlGeneric.get(0);
                    session.delete(hhs);
                }
            }
            // Delete value
            session.delete(houseValuePack.getValue());

            // Handle images and messages
            if (mergedHouse.getImagesByIdHouse().size() > 0) {
                List genericImages = null;
                genericImages = session.createQuery("select I from ImagesEntity as I where I.houseByIdHouse.idHouse = ?")
                        .setInteger(0, mergedHouse.getIdHouse()).list();
                for (Object genImg : genericImages) {
                    session.delete(genImg);
                }
            }
            if (mergedHouse.getMessagesByIdHouse().size() > 0) {
                List genericMessages = null;
                genericMessages = session.createQuery("select M from MessagesEntity as M where M.idHouse = ?")
                        .setInteger(0, mergedHouse.getIdHouse()).list();
                for (Object genMsg : genericMessages) {
                    session.delete(genMsg);
                }
            }

            // Delete house entity
            HouseEntity loadedHouseEntity = (HouseEntity) session.load(HouseEntity.class, mergedHouse.getIdHouse());
            session.delete(loadedHouseEntity);

            // Delete address and location
            // By deleting the address the location is deleted as well
            int locationID = mergedHouse.getLocationByIdLocation().getIdLocation();
            int addressID = mergedHouse.getLocationByIdLocation().getIdAddress();
            AddressEntity addressEntity = (AddressEntity) session.createQuery("select A from AddressEntity as A where A.idAddress = ?")
                    .setInteger(0, addressID).list().get(0);
            session.delete(addressEntity);

            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOG.error("deleteHouse - Hibernate error: " + e.getMessage());
            wasSuccessful = false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if (wasSuccessful)
            LOG.debug("deleteHouse: deleted house with id: [ " + houseValuePack.getHouse().getIdHouse() + " ]");
        return wasSuccessful;
    }

    public static HouseTransactionsDAOImpl getHouseTransactionDAO() {
        return new HouseTransactionsDAOImpl();
    }

    @Override
    public List<HouseValuePack> housesOfferedFromUser(Integer userID, boolean isSalesman) {
        ArrayList<HouseValuePack> result = null;
        Session session = null;
        Transaction tx = null;
        List houseValueGeneric = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            Query query = null;
            if (isSalesman)
                query = session.createQuery("select H, V from HouseHasSalesmanEntity as T, HouseEntity as H, ValueEntity as V " +
                        "where T.idUser = ? and T.idHouse = H.idHouse and T.idValue = V.idValue");
            else
                query = session.createQuery("select H, V from HouseHasLessorEntity as T, HouseEntity as H, ValueEntity as V " +
                        "where T.idUser = ? and T.idHouse = H.idHouse and T.idValue = V.idValue");
            query.setInteger(0, userID);
            houseValueGeneric = query.list();
            if ((houseValueGeneric != null) && (houseValueGeneric.size() != 0)) {
                result = new ArrayList<HouseValuePack>();
                for (Object houseValuePack: houseValueGeneric) {
                    Object[] houseValuePackArray = (Object[]) houseValuePack;
                    HouseEntity house = (HouseEntity) houseValuePackArray[0];
                    // Initialize Location Entity of house
                    Hibernate.initialize(house.getLocationByIdLocation());
                    // Initialize Address Entity of location
                    Hibernate.initialize(house.getLocationByIdLocation().getAddressByIdAddress());
                    // Initialize Images collection
                    Hibernate.initialize(house.getImagesByIdHouse());
                    // Initialize Messages collection
                    Hibernate.initialize(house.getMessagesByIdHouse());
                    ValueEntity value = (ValueEntity) houseValuePackArray[1];
                    result.add(new HouseValuePack(house, value));
                }
                String role;
                if (isSalesman)
                    role = "Salesman";
                else
                    role = "Lessor";
                LOG.debug(role + " [" + userID + "] has " + houseValueGeneric.size() + " houses listed");
                return result;
            }
        } catch (HibernateException e) {
            LOG.error("housesOfferedFromUser - Hibernate error: " + e.getMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return null;
    }

    @Override
    public boolean updateHouseValue(ValueEntity value) {
        Session session = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            // Persist value
            session.update(value);
            tx.commit();
        } catch (HibernateException e) {
            if(tx != null)
                tx.rollback();
            LOG.error("updateHouseValue - Hibernate error: " + e.getLocalizedMessage());
            return false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return true;
    }

    @Override
    public List<Latlng> getCoordinatesHousesUser(Integer userID, boolean isSalesman) {
        ArrayList<Latlng> result = null;
        Session session = null;
        Transaction tx = null;
        List generic = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            Query query = null;
            if (isSalesman)
                query = session.createQuery("select locationByIdLocation from LocationEntity as L, HouseEntity as H, " +
                        "HouseHasSalesmanEntity as S where S.idUser = ? and S.idHouse = H.idHouse");
            else
                query = session.createQuery("select locationByIdLocation from LocationEntity as L, HouseEntity as H, " +
                        "HouseHasLessorEntity as S where S.idUser = ? and S.idHouse = H.idHouse");
            query.setInteger(0, userID);
            generic = query.list();
            if ((generic != null) && (generic.size() != 0)) {
                result = new ArrayList<Latlng>();
                for (Object obj: generic) {
                    LocationEntity location = (LocationEntity) obj;
                    result.add(new Latlng(location.getLatitude(), location.getLongitude()));
                }
                String role;
                if (isSalesman)
                    role = "Salesman";
                else
                    role = "Lessor";
                LOG.debug(role + " [" + userID + "] has " + generic.size() + " houses listed");
                return result;
            }
        } catch (HibernateException e) {
            LOG.error("getCoordinatesHousesUser - Hibernate error: " + e.getLocalizedMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return null;
    }
}
