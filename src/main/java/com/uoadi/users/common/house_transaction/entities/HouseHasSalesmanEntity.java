package com.uoadi.users.common.house_transaction.entities;

import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.offers.entities.SalesmanEntity;

import javax.persistence.*;


@Entity
@Table(name = "house_has_salesman", schema = "", catalog = "myhouse")
public class HouseHasSalesmanEntity {
    private int idHouse;
    private int idValue;
    private int idUser;
    private ValueEntity valueByIdValue;
    private SalesmanEntity salesmanByIdUser;
    private HouseEntity houseByIdHouse;

    @Id
    @Column(name = "idHouse", nullable = false, insertable = true, updatable = true)
    public int getIdHouse() {
        return idHouse;
    }

    public void setIdHouse(int idHouse) {
        this.idHouse = idHouse;
    }

    @Basic
    @Column(name = "idValue", nullable = false, insertable = true, updatable = true)
    public int getIdValue() {
        return idValue;
    }

    public void setIdValue(int idValue) {
        this.idValue = idValue;
    }

    @Basic
    @Column(name = "idUser", nullable = false, insertable = true, updatable = true)
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HouseHasSalesmanEntity that = (HouseHasSalesmanEntity) o;

        if (idHouse != that.idHouse) return false;
        if (idUser != that.idUser) return false;
        if (idValue != that.idValue) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idHouse;
        result = 31 * result + idValue;
        result = 31 * result + idUser;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "idValue", referencedColumnName = "idValue", nullable = false)
    public ValueEntity getValueByIdValue() {
        return valueByIdValue;
    }

    public void setValueByIdValue(ValueEntity valueByIdValue) {
        this.valueByIdValue = valueByIdValue;
    }

    @ManyToOne
    @JoinColumn(name = "idUser", referencedColumnName = "idUser", nullable = false)
    public SalesmanEntity getSalesmanByIdUser() {
        return salesmanByIdUser;
    }

    public void setSalesmanByIdUser(SalesmanEntity salesmanByIdUser) {
        this.salesmanByIdUser = salesmanByIdUser;
    }

    @OneToOne
    @JoinColumn(name = "idHouse", referencedColumnName = "idHouse", nullable = false)
    public HouseEntity getHouseByIdHouse() {
        return houseByIdHouse;
    }

    public void setHouseByIdHouse(HouseEntity houseByIdHouse) {
        this.houseByIdHouse = houseByIdHouse;
    }
}
