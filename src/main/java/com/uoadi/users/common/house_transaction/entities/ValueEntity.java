package com.uoadi.users.common.house_transaction.entities;

import javax.persistence.*;

/**
 *
 */
@Entity
@Table(name = "value", schema = "", catalog = "myhouse")
public class ValueEntity {
    private int idValue;
    private int price;
    private Byte negotiable;

    @Id
    @Column(name = "idValue", nullable = false, insertable = true, updatable = true)
    public int getIdValue() {
        return idValue;
    }

    public void setIdValue(int idValue) {
        this.idValue = idValue;
    }

    @Basic
    @Column(name = "price", nullable = false, insertable = true, updatable = true)
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Basic
    @Column(name = "negotiable", nullable = true, insertable = true, updatable = true)
    public Byte getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(Byte negotiable) {
        this.negotiable = negotiable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValueEntity that = (ValueEntity) o;

        if (idValue != that.idValue) return false;
        if (price != that.price) return false;
        if (negotiable != null ? !negotiable.equals(that.negotiable) : that.negotiable != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idValue;
        result = 31 * result + price;
        result = 31 * result + (negotiable != null ? negotiable.hashCode() : 0);
        return result;
    }
}
