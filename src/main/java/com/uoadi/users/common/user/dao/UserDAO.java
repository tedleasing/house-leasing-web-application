package com.uoadi.users.common.user.dao;

import com.uoadi.users.common.user.entities.CommunicationEntity;
import com.uoadi.users.common.user.entities.NameEntity;
import com.uoadi.users.common.user.entities.UserEntity;

import java.util.List;

public interface UserDAO {
    /*
     * @return true if the user insert successfully in the database else false
     * @param userEntity The user to be inserted
     */
    Integer insertUser(UserEntity user, NameEntity name, CommunicationEntity communication);
    /**
     * Returns the user from the database matching the given
     * username and password, otherwise null
     * @param username The username of the user to be found
     * @param password The password of the user to be found
     * @return The matching user, otherwise null
     */
    UserEntity findUser(String username, String password, Boolean checkPassword);
//    boolean updateUser(UserEntity userEntity);
//    boolean deleteUser(UserEntity userEntity);

    /**
     * Function that populates a list containing the
     * user IDs of all the admins
     * @return
     */
    List<Integer> getAdminIDs();

    /**
     * Returns all the users from the database
     * @return A List of UserEntity objects
     */
    List<UserEntity> getAllOtherUsers(String adminUserName);

    /**
     * Returns all the users that haven't
     * been approved by the admin yet
     * @return
     */
    List<UserEntity> getUnapprovedUsers();

    /**
     * Takes care of loading the lazy object
     * @param user The user entity that its
     *             communication entity will
     *             be loaded
     * @return True on success, false otherwise
     */
    boolean loadCommunication(UserEntity user);

    /**
     *
     * @param element is an attribute of CommunicationEntity
     * @param type define which attribute is the para element
     * @return true if there is a record with an element of this type
     *  otherwise return false
     */
    Boolean findCommunication(String element, int type);

    /**
     *
     * @param id of a communication record
     * @return a CommunicationEntity with the above id
     */
    CommunicationEntity getCommunicationEntity(Integer id);

    /**
     *
     * @param id id of a name record
     * @return a NameEntity with the above id
     */
    NameEntity getNameEntity(Integer id);

    /**
     * Add the user to the table of type 'role' and update the user table
     * @param user
     * @param role
     * @return true if the function terminate properly otherwise false
     */
    Boolean updateUser(UserEntity user, String role);

    /**
     * Update the figures of the user
     * @param user
     * @param communication
     * @param name
     * @param preUser
     * @return true if the function terminate properly otherwise false
     */
    Boolean update(UserEntity user, CommunicationEntity communication,
                          NameEntity name, UserEntity preUser);
}
