package com.uoadi.users.common.user.dao;

import com.uoadi.hibernate_related.HibernateUtil;
import com.uoadi.users.admin.entities.AdminEntity;
import com.uoadi.users.demands.entities.BuyerEntity;
import com.uoadi.users.demands.entities.LesseeEntity;
import com.uoadi.users.offers.entities.LessorEntity;
import com.uoadi.users.offers.entities.SalesmanEntity;
import com.uoadi.users.common.user.entities.CommunicationEntity;
import com.uoadi.users.common.user.entities.NameEntity;
import com.uoadi.users.common.user.entities.UserEntity;
import org.hibernate.*;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for all the
 * CRUD actions using Hibernate
 */
public class UserDAOImpl implements UserDAO, Serializable {
    static final Logger LOG = LoggerFactory.getLogger(UserDAOImpl.class);
    private static final long serialVersionUID = -6385382478961533746L;

    public static UserDAOImpl getUserDAOImpl() {
        return new UserDAOImpl();
    }

    @Override
    public UserEntity findUser(String username, String password, Boolean checkPassword) {
        UserEntity result = null;
        Session session = null;
        Transaction tx = null;
        List usersGeneric = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            if(!checkPassword) {
                usersGeneric = session.createQuery("from UserEntity as U where U.username = ?")
                        .setString(0, username)
                        .list();
                if(usersGeneric != null && usersGeneric.size() == 0) {
                    LOG.info("User [" + username + "] already exists");
                    return null;
                }
                return (UserEntity) (usersGeneric != null && usersGeneric.size() > 0 ? usersGeneric.get(0) : null);
            }
            else {
                usersGeneric = session.createQuery("from UserEntity as U where U.username = ? and U.password = ?")
                        .setString(0, username)
                        .setString(1, password)
                        .list();
                if ((usersGeneric != null) && (usersGeneric.size() != 0)) {
                    LOG.info("User [" + username + "] logged in");
                    return (UserEntity) usersGeneric.get(0);
                }
                return null;
            }
        } catch (HibernateException e) {
            LOG.error("findUser - Hibernate error");
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
    }

    @Override
    public List<Integer> getAdminIDs() {
        ArrayList<Integer> adminIDs = null;
        List genericAdminIDs = null;
        Session session = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            genericAdminIDs = session.createQuery("from AdminEntity").list();
        } catch (HibernateException e) {
            LOG.error("getAdminIDs - Hibernate error");
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if ((genericAdminIDs != null) && (genericAdminIDs.size() != 0)) {
            adminIDs = new ArrayList<Integer>();
            for(Object adminEntity: genericAdminIDs){
                int adminID = ((AdminEntity) adminEntity).getIdUser();
                adminIDs.add(adminID);
            }
            return adminIDs;
        }
        return null;
    }

    @Override
    public List<UserEntity> getUnapprovedUsers() {
        ArrayList<UserEntity> users = null;
        List genericUsers = null;
        Session session = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            genericUsers = session.createQuery("from UserEntity as U WHERE U.approved = 0").list();
            for (Object user: genericUsers) {
                Hibernate.initialize(((UserEntity) user).getCommunicationByIdCommunication());
                Hibernate.initialize(((UserEntity)user).getNameByIdName());
            }
        } catch (HibernateException e) {
            LOG.error("getUnapprovedUsers - Hibernate error: " + e.getLocalizedMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if ((genericUsers != null) && (genericUsers.size() != 0 )) {
            users = new ArrayList<UserEntity>();
            for(Object userEntity: genericUsers){
                UserEntity user = (UserEntity) userEntity;
                users.add(user);
            }
            return users;
        }
        return null;
    }

    @Override
    public List<UserEntity> getAllOtherUsers(String adminUserName) {
        ArrayList<UserEntity> users = null;
        List genericUsers = null;
        Session session = null;
        Transaction tx = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            genericUsers = session.createQuery("from UserEntity as U where U.username <> ?")
                    .setString(0,adminUserName)
                    .list();
            for (Object user: genericUsers) {
                Hibernate.initialize(((UserEntity)user).getCommunicationByIdCommunication());
                Hibernate.initialize(((UserEntity)user).getNameByIdName());
            }
        } catch (HibernateException e) {
            LOG.error("getAllOtherUsers - Hibernate error: " + e.getLocalizedMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if ((genericUsers != null) && (genericUsers.size() != 0)) {
            users = new ArrayList<UserEntity>();
            for(Object userEntity: genericUsers){
                UserEntity user = (UserEntity) userEntity;
                users.add(user);
            }
            return users;
        }
        return null;
    }

    @Override
    public Integer insertUser(UserEntity user, NameEntity name, CommunicationEntity communication) {
        Session session = null;
        Transaction tx = null;
        Integer maxId;
        BuyerEntity buyerEntity = new BuyerEntity();
        SalesmanEntity salesmanEntity = new SalesmanEntity();
        LessorEntity lessorEntity = new LessorEntity();
        LesseeEntity lesseeEntity = new LesseeEntity();
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();

            Criteria crit = session.createCriteria(UserEntity.class);
            crit.setProjection(Projections.max("idUser"));
            List teams = crit.list();
            maxId = (Integer) teams.get(0);
            maxId = (maxId==null) ? 1 : maxId+1;
            user.setIdUser(maxId+1);

            if(name != null) {
                crit = session.createCriteria(NameEntity.class);
                crit.setProjection(Projections.max("idName"));
                teams = crit.list();
                maxId = (Integer) teams.get(0);
                maxId = (maxId == null) ? 1 : maxId + 1;
                name.setIdName(maxId);
                user.setNameByIdName(name);
                session.save(name);
            }

            if(communication != null) {
                crit = session.createCriteria(CommunicationEntity.class);
                crit.setProjection(Projections.max("idCommunication"));
                teams = crit.list();
                maxId = (Integer) teams.get(0);
                maxId = (maxId == null) ? 1 : maxId + 1;
                communication.setIdCommunication(maxId);
                user.setCommunicationByIdCommunication(communication);
                session.save(communication);
            }
            session.save(user);
            if(user.getIsBuyer() == 1) {
                buyerEntity.setIdUser(user.getIdUser());
                buyerEntity.setUserByIdUser(user);
                session.save(buyerEntity);
            }
            if(user.getIsSalesman() == 1) {
                salesmanEntity.setIdUser(user.getIdUser());
                salesmanEntity.setUserByIdUser(user);
                session.save(salesmanEntity);
            }
            if(user.getIsLeessor() == 1) {
                lessorEntity.setIdUser(user.getIdUser());
                session.save(lessorEntity);
            }
            if(user.getIsLessee() == 1) {
                lesseeEntity.setIdUser(user.getIdUser());
                session.save(lesseeEntity);
            }

            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOG.error("insertUser - Hibernate error" + e.getMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        LOG.debug("insertUser: Insert user with username: [ " + user.getUsername() + " ]");
        return user.getIdUser();
    }

    private List communicationQuery(Session session, String element, int type) {
        switch (type) {
            case 1:
                return session.createQuery("select eMail from CommunicationEntity" +
                        " as com where com.eMail=?")
                        .setString(0,element)
                        .list();
            case 2:
                return session.createQuery("select mobile from CommunicationEntity" +
                        " as com where com.mobile=?")
                        .setString(0,element)
                        .list();
            case 3:
                return session.createQuery("select telephone from CommunicationEntity" +
                        " as com where com.telephone=?")
                        .setString(0,element)
                        .list();
            default:
                return null;
        }
    }

    private Boolean equalCommunication(String tmpElement, String element, int type) {
        switch (type) {
            case 1:
                if (tmpElement.equals(element)) {
                    LOG.debug("findEmail: User tried to duplicate email: [ " + element + " ]");
                    return true;
                }
                return false;
            case 2:
                if (tmpElement.equals(element)) {
                    LOG.debug("findMobile: User tried to duplicate mobile: [ " + element + " ]");
                    return true;
                }
                return false;
            case 3:
                if (tmpElement.equals(element)) {
                    LOG.debug("findTelephone: User tried to duplicate telephone: [ " + element + " ]");
                    return true;
                }
                return false;
            default:
                return null;
        }
    }

    @Override
    public Boolean findCommunication(String element, int type) {
        CommunicationEntity result = null;
        Session session = null;
        Transaction tx = null;
        List Generic = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            Generic = communicationQuery(session, element, type);
        } catch (HibernateException e) {
            LOG.error("findEmail - Hibernate error" + e.getMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if (Generic != null) {
            for (Object tmp: Generic) {
                String com = (String) tmp;
                if(equalCommunication(com, element, type))
                    return true;
            }
        }
        return false;
    }

    @Override
    public CommunicationEntity getCommunicationEntity(Integer id) {
        Session session = null;
        Transaction tx = null;
        List Generic = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            Generic = session.createQuery("from CommunicationEntity" +
                    " as com where com.id=?")
                    .setInteger(0,id)
                    .list();
        } catch (HibernateException e) {
            LOG.error("getCommunication - Hibernate error" + e.getMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if (Generic != null) {
            for (Object tmp: Generic) {
                return (CommunicationEntity) tmp;
            }
        }
        return null;
    }

    @Override
    public NameEntity getNameEntity(Integer id) {
        Session session = null;
        Transaction tx = null;
        List Generic = null;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();
            Generic = session.createQuery("from NameEntity" +
                    " as name where name.idName = ?")
                    .setInteger(0,id)
                    .list();
        } catch (HibernateException e) {
            LOG.error("getName - Hibernate error" + e.getMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        if (Generic != null) {
            for (Object tmp: Generic) {
                return (NameEntity) tmp;
            }
        }
        return null;
    }

    @Override
    public Boolean updateUser(UserEntity user, String role) {
        Session session = null;
        Transaction tx = null;
        Integer maxId;
        BuyerEntity buyerEntity = new BuyerEntity();
        SalesmanEntity salesmanEntity = new SalesmanEntity();
        LessorEntity lessorEntity = new LessorEntity();
        LesseeEntity lesseeEntity = new LesseeEntity();
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();

            if(role.equals("buyer")) {
                buyerEntity.setIdUser(user.getIdUser());
                buyerEntity.setUserByIdUser(user);
                session.saveOrUpdate(buyerEntity);
            }

            if(role.equals("salesman")) {
                salesmanEntity.setIdUser(user.getIdUser());
                salesmanEntity.setUserByIdUser(user);
                session.saveOrUpdate(salesmanEntity);
            }

            if(role.equals("lessor")) {
                lessorEntity.setIdUser(user.getIdUser());
                lessorEntity.setUserByIdUser(user);
                session.saveOrUpdate(lessorEntity);
            }

            if(role.equals("lessee")) {
                lesseeEntity.setIdUser(user.getIdUser());
                session.saveOrUpdate(lesseeEntity);
            }
            session.update(user);
            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOG.error("updateUser/role - Hibernate error" + e.getMessage());
            return false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        LOG.debug("updateUser/role: Update user with username: [ " + user.getUsername() + " ]");
        return true;
    }

    @Override
    public Boolean update(UserEntity user, CommunicationEntity communication,
                          NameEntity name, UserEntity preUser) {
        Session session = null;
        Transaction tx = null;
        Integer maxId;
        BuyerEntity buyerEntity = new BuyerEntity();
        SalesmanEntity salesmanEntity = new SalesmanEntity();
        LessorEntity lessorEntity = new LessorEntity();
        LesseeEntity lesseeEntity = new LesseeEntity();
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.getTransaction();
            // Define transaction boundaries
            tx.begin();

            if(name != null) {
                if(name.getIdName() == 0) {
                    Criteria crit = session.createCriteria(NameEntity.class);
                    crit.setProjection(Projections.max("idName"));
                    List teams = crit.list();
                    maxId = (Integer) teams.get(0);
                    maxId = (maxId == null) ? 1 : maxId + 1;
                    name.setIdName(maxId);
                    user.setNameByIdName(name);
                    session.save(name);
                }
                else
                    session.update(name);
            }

            session.update(communication);
            user.setCommunicationByIdCommunication(communication);

            if(user.getIsBuyer() == 1 && preUser.getIsBuyer() == 0) {
                buyerEntity.setIdUser(user.getIdUser());
                buyerEntity.setUserByIdUser(user);
                session.saveOrUpdate(buyerEntity);
            }
//            else if(user.getIsBuyer() == 0 && preUser.getIsBuyer() == 1) {
//                buyerEntity.setIdUser(preUser.getIdUser());
//                buyerEntity.setUserByIdUser(preUser);
//                session.delete(buyerEntity);
//            }

            if(user.getIsSalesman() == 1 && preUser.getIsSalesman() == 0) {
                salesmanEntity.setIdUser(user.getIdUser());
                salesmanEntity.setUserByIdUser(user);
                session.saveOrUpdate(salesmanEntity);
            }
//            else if(user.getIsSalesman() == 0 && preUser.getIsSalesman() == 1) {
//                salesmanEntity.setIdUser(preUser.getIdUser());
//                salesmanEntity.setUserByIdUser(preUser);
//                session.delete(salesmanEntity);
//            }

            if(user.getIsLeessor() == 1 && preUser.getIsLeessor() == 0) {
                lessorEntity.setIdUser(user.getIdUser());
                lessorEntity.setUserByIdUser(user);
                session.saveOrUpdate(lessorEntity);
            }
//            else if(user.getIsLeessor() == 0 && preUser.getIsLeessor() == 1) {
//                lessorEntity.setIdUser(preUser.getIdUser());
//                lessorEntity.setUserByIdUser(preUser);
//                session.delete(lessorEntity);
//            }

            if(user.getIsLessee() == 1 && preUser.getIsLessee() == 0) {
                lesseeEntity.setIdUser(user.getIdUser());
                session.saveOrUpdate(lesseeEntity);
            }
//            else if(user.getIsLessee() == 0 && preUser.getIsLessee() == 1) {
//                lesseeEntity.setIdUser(preUser.getIdUser());
//                session.delete(lesseeEntity);
//            }
            session.update(user);

            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOG.error("updateUser - Hibernate error" + e.getMessage());
            return null;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        LOG.debug("updateUser: Update user with username: [ " + user.getUsername() + " ]");
        return true;
    }

    @Override
    public boolean loadCommunication(UserEntity user) {
        Session session = null;
        Transaction tx = null;
        boolean successful = true;
        try {
            // Get a session and a transaction
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            // Re attach the (detached) userEntity to the current session
            tx = session.getTransaction();
            tx.begin();
            UserEntity reAttachedUser = (UserEntity) session.merge(user);
            // Get the field and initialize it
            Field communication = user.getClass().getDeclaredField("communicationByIdCommunication");
            communication.setAccessible(true);
            Object objectToInitialize = communication.get(reAttachedUser);
            Hibernate.initialize(objectToInitialize);
        } catch (HibernateException e) {
            LOG.error("loadCommunication - Hibernate error: " + e.getMessage());
            successful = false;
        } catch (NoSuchFieldException e) {
            LOG.error("loadCommunication: " + e.getMessage());
            successful = false;
        } catch (IllegalAccessException e) {
            LOG.error("loadCommunication: " + e.getMessage());
            successful = false;
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return successful;
    }
}
