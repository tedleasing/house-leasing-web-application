package com.uoadi.users.common.user.entities;

import javax.persistence.*;

/**
 *
 */
@Entity
@Table(name = "communication", schema = "", catalog = "myhouse")
public class CommunicationEntity {
    private int idCommunication;
    private String telephone;
    private String mobile;
    private String eMail;

    public CommunicationEntity() {
    }

    public CommunicationEntity(CommunicationEntity com) {
        if(com != null) {
            idCommunication = com.getIdCommunication();
            telephone = com.getTelephone();
            mobile = com.getMobile();
            eMail = com.geteMail();
        }
    }

    @Id
    @Column(name = "idCommunication", nullable = false, insertable = true, updatable = true)
    public int getIdCommunication() {
        return idCommunication;
    }

    public void setIdCommunication(int idCommunication) {
        this.idCommunication = idCommunication;
    }

    @Basic
    @Column(name = "telephone", nullable = true, insertable = true, updatable = true, length = 10)
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "mobile", nullable = true, insertable = true, updatable = true, length = 10)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "email", nullable = false, insertable = true, updatable = true, length = 45)
    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommunicationEntity that = (CommunicationEntity) o;

        if (idCommunication != that.idCommunication) return false;
        if (eMail != null ? !eMail.equals(that.eMail) : that.eMail != null) return false;
        if (mobile != null ? !mobile.equals(that.mobile) : that.mobile != null) return false;
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCommunication;
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
        result = 31 * result + (eMail != null ? eMail.hashCode() : 0);
        return result;
    }
}
