package com.uoadi.users.common.user.entities;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Table(name = "name", schema = "", catalog = "myhouse")
public class NameEntity {
    private int idName;
    private String name;
    private String surname;
    private String father;
    private Collection<UserEntity> usersByIdName;

    public NameEntity() {

    }

    public NameEntity(NameEntity nameEntity) {
        if(nameEntity != null) {
            idName = nameEntity.getIdName();
            name = nameEntity.getName();
            surname = nameEntity.getSurname();
            father = nameEntity.getFather();
        }
    }

    @Id
    @Column(name = "idName", nullable = false, insertable = true, updatable = true)
    /*
     * @return int
     */
    public int getIdName() {
        return idName;
    }

    public void setIdName(int idName) {
        this.idName = idName;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname", nullable = false, insertable = true, updatable = true, length = 45)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "father", nullable = false, insertable = true, updatable = true, length = 45)
    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NameEntity that = (NameEntity) o;

        if (idName != that.idName) return false;
        if (father != null ? !father.equals(that.father) : that.father != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idName;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "nameByIdName")
    public Collection<UserEntity> getUsersByIdName() {
        return usersByIdName;
    }

    public void setUsersByIdName(Collection<UserEntity> usersByIdName) {
        this.usersByIdName = usersByIdName;
    }
}
