package com.uoadi.users.common.user.entities;


import javax.persistence.*;

@Entity
@Table(name = "user", schema = "", catalog = "myhouse")
public class UserEntity {
    private int idUser;
    private String username;
    private String password;
    private byte approved;
    private byte isBuyer;
    private byte isAdmin;
    private byte isLessee;
    private byte isLeessor;
    private byte isSalesman;
    private Byte requestedReset;
    private String salt;
    private CommunicationEntity communicationByIdCommunication;
    private NameEntity nameByIdName;

    public UserEntity() {
        communicationByIdCommunication = new CommunicationEntity();
        nameByIdName = new NameEntity();
    }

    public UserEntity(UserEntity user) {
        if(user != null) {
            idUser = user.getIdUser();
            username = user.getUsername();
            password = user.getPassword();
            salt = user.getSalt();
            approved = user.getApproved();
            isBuyer = user.getIsBuyer();
            isAdmin = user.getIsAdmin();
            isLessee = user.getIsLessee();
            isLeessor = user.getIsLeessor();
            isSalesman = user.getIsSalesman();
            requestedReset = user.getRequestedReset();
            communicationByIdCommunication = new CommunicationEntity(user.getCommunicationByIdCommunication());
            nameByIdName = new NameEntity(user.getNameByIdName());
        }
    }

    @Id
    @Column(name = "idUser", nullable = false, insertable = true, updatable = true)
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "username", nullable = false, insertable = true, updatable = true, length = 45)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = true, insertable = true, updatable = true, length = 32)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "approved", nullable = false, insertable = true, updatable = true)
    public byte getApproved() {
        return approved;
    }

    public void setApproved(byte approved) {
        this.approved = approved;
    }

    @Basic
    @Column(name = "isBuyer", nullable = false, insertable = true, updatable = true)
    public byte getIsBuyer() {
        return isBuyer;
    }

    public void setIsBuyer(byte isBuyer) {
        this.isBuyer = isBuyer;
    }

    @Basic
    @Column(name = "isAdmin", nullable = false, insertable = true, updatable = true)
    public byte getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(byte isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Basic
    @Column(name = "isLessee", nullable = false, insertable = true, updatable = true)
    public byte getIsLessee() {
        return isLessee;
    }

    public void setIsLessee(byte isLessee) {
        this.isLessee = isLessee;
    }

    @Basic
    @Column(name = "isLeessor", nullable = false, insertable = true, updatable = true)
    public byte getIsLeessor() {
        return isLeessor;
    }

    public void setIsLeessor(byte isLeessor) {
        this.isLeessor = isLeessor;
    }

    @Basic
    @Column(name = "isSalesman", nullable = false, insertable = true, updatable = true)
    public byte getIsSalesman() {
        return isSalesman;
    }

    public void setIsSalesman(byte isSalesman) {
        this.isSalesman = isSalesman;
    }

    @Basic
    @Column(name = "requestedReset", nullable = true, insertable = true, updatable = true)
    public Byte getRequestedReset() {
        return requestedReset;
    }

    public void setRequestedReset(Byte requestedReset) {
        this.requestedReset = requestedReset;
    }

    @Basic
    @Column(name = "salt", nullable = true, insertable = true, updatable = true, length = 32)
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (approved != that.approved) return false;
        if (idUser != that.idUser) return false;
        if (isAdmin != that.isAdmin) return false;
        if (isBuyer != that.isBuyer) return false;
        if (isLeessor != that.isLeessor) return false;
        if (isLessee != that.isLessee) return false;
        if (isSalesman != that.isSalesman) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (requestedReset != null ? !requestedReset.equals(that.requestedReset) : that.requestedReset != null)
            return false;
        if (salt != null ? !salt.equals(that.salt) : that.salt != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idUser;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (int) approved;
        result = 31 * result + (int) isBuyer;
        result = 31 * result + (int) isAdmin;
        result = 31 * result + (int) isLessee;
        result = 31 * result + (int) isLeessor;
        result = 31 * result + (int) isSalesman;
        result = 31 * result + (requestedReset != null ? requestedReset.hashCode() : 0);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "idCommunication", referencedColumnName = "idCommunication", nullable = false)
    public CommunicationEntity getCommunicationByIdCommunication() {
        return communicationByIdCommunication;
    }

    public void setCommunicationByIdCommunication(CommunicationEntity communicationByIdCommunication) {
        this.communicationByIdCommunication = communicationByIdCommunication;
    }

    @ManyToOne
    @JoinColumn(name = "idName", referencedColumnName = "idName")
    public NameEntity getNameByIdName() {
        return nameByIdName;
    }

    public void setNameByIdName(NameEntity nameByIdName) {
        this.nameByIdName = nameByIdName;
    }
}
