package com.uoadi.users.common.user.filters;

import com.uoadi.users.common.user.view.UserStateBean;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/index")
public class LoginUser implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        UserStateBean userState = (UserStateBean) request.getSession().getAttribute("userStateBean");
        // If the user is logged in and tries to access the /index.xhtml file using a URL
        // Redirect him to select-role
        if (userState != null && userState.isLoggedIn()) {
            response.sendRedirect(request.getContextPath() + "/user/select-role.xhtml");
        }
        else
            filterChain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
