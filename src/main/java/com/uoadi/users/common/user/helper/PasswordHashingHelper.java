package com.uoadi.users.common.user.helper;

import com.uoadi.users.common.user.entities.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class PasswordHashingHelper {
    private final static int ITERATION_NUMBER = 1000;
    static final Logger LOG = LoggerFactory.getLogger(PasswordHashingHelper.class);

    /**
     * Receives a user, hashes the password and updates
     * this member with the digest, and also sets the salt
     * @param user UserEntity The user whose password will be hashed
     * @return boolean True on success, false otherwise
     */
    public static boolean hashPassword(UserEntity user) {
        // Handle password hashing
        SecureRandom random = null;
        boolean saltGenerated = true;
        byte[] bSalt = null;
        try {
            random = SecureRandom.getInstance("SHA1PRNG");
            // Salt generation 64 bits long
            bSalt = new byte[8];
            random.nextBytes(bSalt);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            saltGenerated = false;
        }
        boolean digestGenerated = true;
        if (saltGenerated) {
            // Digest computation
            byte[] bDigest;
            try {
                bDigest = getHash(ITERATION_NUMBER, user.getPassword(), bSalt);
                String sDigest = byteToBase64(bDigest);
                String sSalt = byteToBase64(bSalt);
                user.setPassword(sDigest);
                user.setSalt(sSalt);
            } catch (UnsupportedEncodingException e) {
                LOG.error("UTF-8 is unsupported: " + e.getMessage());
                digestGenerated = false;
            } catch (NoSuchAlgorithmException e) {
                LOG.error("Unsupported hashing algorithm: " + e.getMessage());
                digestGenerated = false;
            }
        }
        return saltGenerated && digestGenerated;
    }

    /**
     * Will check if the given password matches with the one from the
     * password field of the User, using the salt of the user
     * @param givenPassword String The password that the user has typed (clear text)
     * @param user UserEntity The user whose password must be checked against the
     *             given one
     * @return boolean True if they match, false otherwise
     */
    public static boolean passwordsMatch(String givenPassword, UserEntity user) {
        byte[] bGivenPasswordDigest = new byte[0], currentPasswordDigest = new byte[0];
        boolean calculatedDigest = true;
        try {
            bGivenPasswordDigest = getHash(ITERATION_NUMBER, givenPassword, base64ToByte(user.getSalt()));
            currentPasswordDigest = base64ToByte(user.getPassword());
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Unsupported algorithm: " + e.getMessage());
            calculatedDigest = false;
        } catch (IOException e) {
            LOG.error("Could not decode password: " + e.getMessage());
            calculatedDigest = false;
        }
        return calculatedDigest && Arrays.equals(bGivenPasswordDigest, currentPasswordDigest);
    }

    /**
     * Returns the digest, combining the password
     * the number of iterations and a salt
     * @param iterationsNb int The number of iterations of the algorithm
     * @param password String The password to encrypt
     * @param salt byte[] The salt to use
     * @return  byte[] The digested password
     * @throws java.security.NoSuchAlgorithmException If the algorithm doesn't exist
     * @throws java.io.UnsupportedEncodingException If the encoding UTF-8 isn't supported
     */
    public static byte[] getHash(int iterationsNb, String password, byte[] salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.reset();
        digest.update(salt);
        byte[] input = digest.digest(password.getBytes("UTF-8"));
        for (int i = 0; i < iterationsNb; i++) {
            digest.reset();
            input = digest.digest(input);
        }
        return input;
    }

    /**
     * From a base 64 representation, returns the corresponding byte[]
     * @param data String The base64 representation
     * @return byte[]
     * @throws java.io.IOException
     */
    public static byte[] base64ToByte(String data) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        return decoder.decodeBuffer(data);
    }

    /**
     * From a byte[] returns a base 64 representation
     * @param data byte[]
     * @return String
     */
    public static String byteToBase64(byte[] data) {
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }
}
