package com.uoadi.users.common.user.helper;

import com.uoadi.users.common.user.entities.CommunicationEntity;
import com.uoadi.users.common.user.entities.NameEntity;
import com.uoadi.users.common.user.entities.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UsersElements {
    static final Logger LOG = LoggerFactory.getLogger(UsersElements.class);
    private String confirmPassword;
    private byte isBuyer;
    private byte isLessee;
    private byte isLessor;
    private byte isSalesman;
    private byte isAdmin;
    private NameEntity name;
    private CommunicationEntity communication;
    private UserEntity user;

    public UsersElements() {
        name = new NameEntity();
        communication  = new CommunicationEntity();
        user = new UserEntity();
        name.setIdName(1);
        communication.setIdCommunication(1);
        confirmPassword = "";
    }

    public void prepareCommunicationDAO() {
        String mobile = communication.getMobile();
        String telephone = communication.getTelephone();
        if(mobile != null && mobile.equals(""))
            communication.setMobile(null);
        if(telephone != null && telephone.equals(""))
            communication.setTelephone(null);
    }

    public void prepareNameDAO() {
        String surname = this.name.getSurname();
        String father = this.name.getFather();
        if(name.getName() == null || name.getName().equals(""))
            name.setName(null);
        if(surname == null || surname.equals(""))
            name.setSurname(null);
        if(father == null || father.equals(""))
            name.setFather(null);
        if(name == null && surname == null && father == null) {
            this.name = null;
        }
    }

    public void prepareUserDAO() {
        if(user.getUsername().equals("")) {
            user.setUsername(null);
        }
        user.setIsBuyer(isBuyer);
        user.setIsLessee(isLessee);
        user.setIsLeessor(isLessor);
        user.setIsSalesman(isSalesman);
        if(user.getNameByIdName().getIdName() == 0)
            user.setNameByIdName(null);
        user.setIsAdmin(isAdmin);
    }

    public void setRoles() {
        isAdmin = user.getIsAdmin();
        isBuyer = user.getIsBuyer();
        isLessee = user.getIsLessee();
        isLessor = user.getIsLeessor();
        isSalesman = user.getIsSalesman();
    }

    public void setUsersAttributes() {
        user.setCommunicationByIdCommunication(communication);
        user.setNameByIdName(name);
    }

    /*getters/setters*/
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public NameEntity getName() {
        return name;
    }

    public void setName(NameEntity name) {
        this.name = name;
    }

    public CommunicationEntity getCommunication() {
        return communication;
    }

    public void setCommunication(CommunicationEntity communication) {
        this.communication = communication;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Boolean getIsBuyer() {

        if(isBuyer == 1)
            return true;
        return false;
    }

    public void setIsBuyer(Boolean isBuyer) {
        if(isBuyer)
            this.isBuyer = 1;
        else
            this.isBuyer = 0;
    }

    public void setIsLessee(Boolean isLessee) {
        if(isLessee)
            this.isLessee = 1;
        else
            this.isLessee = 0;
    }

    public void setIsSalesman(Boolean isSalesman) {
        if(isSalesman)
            this.isSalesman = 1;
        else
            this.isSalesman = 0;
    }

    public void setIsLessor(Boolean isLessor) {
        if(isLessor)
            this.isLessor = 1;
        else
            this.isLessor = 0;
    }

    public void setIsAdmin(Boolean isAdmin) {
        if(isAdmin)
            this.isAdmin = 1;
        else
            this.isAdmin = 0;
    }

    public Boolean getIsLessee() {
        if(isLessee == 1)
            return true;
        return false;
    }

    public Boolean getIsLessor() {
        if(isLessor == 1)
            return true;
        return false;
    }

    public Boolean getIsSalesman() {
        if(isSalesman == 1)
            return true;
        return false;
    }

    public Boolean getIsAdmin() {
        if(isAdmin == 1)
            return true;
        return false;
    }
}
