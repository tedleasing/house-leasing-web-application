package com.uoadi.users.common.user.service;

import com.uoadi.users.common.user.dao.UserDAOImpl;
import com.uoadi.users.common.user.entities.CommunicationEntity;
import com.uoadi.users.common.user.entities.NameEntity;
import com.uoadi.users.common.user.entities.UserEntity;
import com.uoadi.users.common.user.helper.PasswordHashingHelper;
import com.uoadi.users.common.user.helper.UsersElements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

@Stateless
public class UserService implements Serializable {
    static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private static final long serialVersionUID = 3969434478839635415L;
    private final static int ITERATION_NUMBER = 1000;

    private UserDAOImpl userDAO;

    public UserService() {
        userDAO = UserDAOImpl.getUserDAOImpl();
        LOG.info("Got userDAO");
    }

    public Integer insert(UserEntity user, NameEntity name, CommunicationEntity communication) {

        return userDAO.insertUser(user, name, communication);
    }

    public Boolean update(UserEntity user, NameEntity name, CommunicationEntity communication, UserEntity preUser) {
        return userDAO.update(user,communication, name, preUser);
    }

    public Boolean updateUser(UserEntity user, String role) {
        return userDAO.updateUser(user, role);
    }

    /**
     * Calls the find of the dao to search for the user
     * If we're trying to authenticate the user, then if
     * the user does exist, we're hashing the given password
     * and trying to see if the digests match
     * If the user doesn't exist, we still calculate the
     * digests, thus ensuring "bad" behaviour for non-legitimate
     * user (credits to OWASP)
     * @param username The username of the user to be found
     * @param password The password of the user to be found
     * @return The UserEntity that was found, otherwise null
     */
    public UserEntity find(String username, String password, Boolean checkPassword) {
        UserEntity user = null;
        user = userDAO.findUser(username, password, checkPassword);
        String digest = null, salt = null;
        boolean inUserAuthentication = false;
        if ((password != null) && (user != null)) {
            // We're trying to authenticate the user
            inUserAuthentication = true;
            digest = user.getPassword();
            salt = user.getSalt();
            // Database validation
            if ((digest == null) || (salt == null))
                LOG.error("Database inconsistent - Salt or Digested password are altered!");
        }
        else {
            password = "";
            // Time resistant attack - Even if the user doesn't exist, the
            // computation time is equal to the time needed for a legitimate user
            digest = "000000000000000000000000000=";
            salt = "00000000000=";
        }
        byte[] bDigest, bSalt;
        try {
            bDigest = PasswordHashingHelper.base64ToByte(digest);
            bSalt = PasswordHashingHelper.base64ToByte(salt);
            // Computing the new digest
            byte[] new_digest;
            new_digest = PasswordHashingHelper.getHash(ITERATION_NUMBER, password, bSalt);
            boolean doMatch = Arrays.equals(new_digest, bDigest);
            if ((!doMatch) && (inUserAuthentication))
                user = null;
        } catch (IOException e) {
            LOG.error("Failed to use base64 methods: " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Failed to find hashing algorithm: " + e.getMessage());
        }
        return user;
    }

    /**
     * Calls the find of the dao to search for the email
     * @param element The element of the user to be found
     * @param type The type of the element(email, mobile, telephone)
     * @return True if the element was found, otherwise false
     */
    public Boolean findCommunication(String element, int type) {
        return userDAO.findCommunication(element, type);
    }

    public CommunicationEntity getCommunication(Integer id) {
        return userDAO.getCommunicationEntity(id);
    }

    public NameEntity getName(Integer id) {
        return userDAO.getNameEntity(id);
    }

    /**
     * Constructs the outcome that will redirect the users
     * depending on their roles
     * @param loggedInUser The logged in user UserEntity
     * @return The outcome string
     */
    public String handleSingleRedirectCase(UserEntity loggedInUser) {
        // find if the user is an admin
        if (loggedInUser.getIsAdmin() == 1) {
            // double verification
            List<Integer> adminIDs = userDAO.getAdminIDs();
            int loggedInID = loggedInUser.getIsAdmin();
            if (adminIDs.contains(loggedInID))
                return "/admin/home" + "?faces-redirect=true";
        }
        else if ((loggedInUser.getIsBuyer() == 1) || (loggedInUser.getIsLessee() == 1)) {
            return "/demands/landing" + "?faces-redirect=true";
        }
        else if ((loggedInUser.getIsSalesman() == 1) || (loggedInUser.getIsLeessor() == 1)) {
            return "/offers/landing" + "?faces-redirect=true";
        }
        else
            return null;
        return null;
    }


    /**
     * Double checks that the logged in user is an admin by looking up:
     * 1) The boolean field at the UserEntity
     * 2) His userID belongs in the Admin Table
     * @param loggedInUser The logged in user UserEntity
     * @return True if the user is an admin, otherwise false
     */
    public boolean isAdminDoubleVerification(UserEntity loggedInUser) {
        if (loggedInUser == null)
            return false;
        if (loggedInUser.getIsAdmin() == 1) {
            List<Integer> adminIDs = userDAO.getAdminIDs();
            int loggedInID = loggedInUser.getIsAdmin();
            return adminIDs.contains(loggedInID);
        }
        else
            return false;
    }

    /**
     * Wrapper function for returning the unapproved
     * users from the DB
     * @return The list of the unapproved users
     */
    public List<UserEntity> getUnapprovedUsers() {
        return userDAO.getUnapprovedUsers();
    }

    /**
     * Wrapper function for returning all the users
     * @return The list of the users in the DB
     */
    public List<UserEntity> getAllUsers(String adminUserName) {
        return userDAO.getAllOtherUsers(adminUserName);
    }


    /**
     * Wrapper function for lazy loading the
     * communication entity
     * @param user The user entity whose communication
     *             entity will be lazy loaded
     * @return True on success, false otherwise
     */
    public boolean loadCommunication(UserEntity user) {
        return userDAO.loadCommunication(user);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserService)) return false;

        UserService that = (UserService) o;

        if (userDAO != null ? !userDAO.equals(that.userDAO) : that.userDAO != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return userDAO != null ? userDAO.hashCode() : 0;
    }
}
