package com.uoadi.users.common.user.view;


import com.uoadi.users.common.user.entities.NameEntity;
import com.uoadi.users.common.user.entities.UserEntity;
import com.uoadi.users.common.user.helper.PasswordHashingHelper;
import com.uoadi.users.common.user.helper.UsersElements;
import com.uoadi.users.common.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

@ManagedBean
@ViewScoped
public class UserProfileView implements Serializable {
    private static final long serialVersionUID = -8481377211230057402L;
    static final Logger LOG = LoggerFactory.getLogger(UserProfileView.class);

    private String curPassword;
    private String newPassword;
    private String preUsername;
    private String prePassword;
    private String preEmail;
    private String prePhone;
    private String preMobile;
    private Byte approved;
    private UsersElements userInfo;

    @EJB
    private UserService userService;
    private UserEntity preUserEntity;

    @ManagedProperty(value = "#{userStateBean.current}")
    UserEntity current;
    Boolean canApprove;

    @PostConstruct
    public void init() {
        userInfo = new UsersElements();
        //take the user whose profile will be presented
        userInfo.setUser((UserEntity) FacesContext.getCurrentInstance().getCurrentInstance().getExternalContext().getSessionMap().get("UserEntities"));
        prepareFieldsCommunicationForPage();
        prepareFieldsNameForPage();
        prepareFieldsUserForPage();
        setPrevalues();
    }

    private void prepareFieldsUserForPage() {
        UserEntity user = userInfo.getUser();
        userInfo.setUsersAttributes();
        preUserEntity = new UserEntity(user);
//        curPassword = password;
        newPassword = "";
        approved = user.getApproved();
        userInfo.setRoles();
    }

    private  void prepareFieldsCommunicationForPage() {
        userInfo.setCommunication(userService.getCommunication(userInfo.getUser().getCommunicationByIdCommunication().
                getIdCommunication()));
    }

    private void prepareFieldsNameForPage() {
        UserEntity user = userInfo.getUser();
        if(user.getNameByIdName() != null) {
            userInfo.setName(userService.getName(user.getNameByIdName().getIdName()));

        } else {
            userInfo.setName(new NameEntity());
        }
    }

    private void prepareUserDAO() {
        UserEntity user = userInfo.getUser();
        user.setApproved(approved);
        userInfo.prepareUserDAO();
    }

    public String updateProfile() {
        Boolean result, doContinue = true;
        UserEntity user = userInfo.getUser();
        String username = user.getUsername();
        String email = user.getCommunicationByIdCommunication().geteMail();
        String mobile = user.getCommunicationByIdCommunication().getMobile();
        String telephone = user.getCommunicationByIdCommunication().getTelephone();
        String message = " υπάρχει ήδη, παρακαλώ ελέξτε το πεδίο ξανά.\nΣε περίπτωση που πιστεύετε ότι κάποιος" +
                " χρησημοποιεί προσωπικά σας στοιχεία παρακαλούμε πολυ επικοινωνίστε άμεσα με το διαχειριστή!!!";
        /*check if a value doesn't exist in the database and is different from the current value of the user's profile*/
        /*check if the username already exist*/
        UserEntity exist = userService.find(username, null, false);
        if(exist != null && !username.equals(preUsername)) {
            exist = null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το όνομα χρήστη υπάρχει ήδη, δοκιμάστε ένα άλλο", ""));
            doContinue = false;
        }
        /*check if the email already exist*/
        result = userService.findCommunication(email, 1);
        if(result && !email.equals(preEmail)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το email " + email + message, ""));
            doContinue = false;
        }
        /*check if the mobile phone already exist*/
        result = userService.findCommunication(mobile, 2);
        if(result && !mobile.equals(preMobile)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το κινητό " + mobile + message, ""));
            doContinue = false;
        }
        /*check if the phone already exist*/
        result = userService.findCommunication(telephone, 3);
        if(result && !telephone.equals(prePhone)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το σταθερό τηλέφωνο" + telephone + message, ""));
            doContinue = false;
        }
        /* Check if the user is an admin or a user who visited his/her profile */
        if(!canApprove) {
            if( (curPassword != null) && (!PasswordHashingHelper.passwordsMatch(curPassword, user) || curPassword.equals("")) ) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Λάθος κωδικός. Παρακαλώ δοκιμάστε ξανά", ""));
                doContinue = false;
            }
        }
        /* Check if the values of the fields are valid */
        if(!doContinue)
            return null;
        userInfo.prepareNameDAO();
        userInfo.prepareCommunicationDAO();
        prepareUserDAO();
        result = userService.update(user, userInfo.getName(), userInfo.getCommunication(), preUserEntity);
        if(result == null) {
            return "/errors/error" + "?faces-redirect=true";
        }
        setPrevalues();
        return "/user/profile" + "?faces-redirect=true";
    }

    private void setPrevalues() {
        preEmail = userInfo.getCommunication().geteMail();
        preMobile = userInfo.getCommunication().getMobile();
        prePassword = userInfo.getUser().getPassword();
        prePhone = userInfo.getCommunication().getTelephone();
        preUsername = userInfo.getUser().getUsername();
        if(preMobile == null)
            preMobile = "";
        if(prePhone == null)
            prePhone = "";
    }

    public void rollback() {
        userInfo.setUser(preUserEntity);
        prepareFieldsCommunicationForPage();
        prepareFieldsNameForPage();
        prepareFieldsUserForPage();
    }

    /*getters/setters*/
    public UsersElements getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UsersElements userInfo) {
        this.userInfo = userInfo;
    }

    public UserEntity getCurrent() {
        return current;
    }

    public void setCurrent(UserEntity current) {
        this.current = current;
    }

    public String getCurPassword() {
        return curPassword;
    }

    public void setCurPassword(String curPassword) {
        this.curPassword = curPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public Boolean getApproved() {
        if(approved==1)
            return true;
        return  false;
    }

    public Boolean getCanApprove() {
        canApprove = (current.getIsAdmin() == 1) && (userInfo.getUser().getIsAdmin() == 0);
        return canApprove;
    }

    public void setCanApprove(Boolean canApprove) {
        this.canApprove = canApprove;
    }

    public void setApproved(Boolean approved) {
        if(approved)
            this.approved = 1;
        else
            this.approved = 0;
    }

    public void setApproved(Byte approved) {
        this.approved = approved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserProfileView that = (UserProfileView) o;

        if (approved != null ? !approved.equals(that.approved) : that.approved != null) return false;
        if (canApprove != null ? !canApprove.equals(that.canApprove) : that.canApprove != null) return false;
        if (curPassword != null ? !curPassword.equals(that.curPassword) : that.curPassword != null) return false;
        if (current != null ? !current.equals(that.current) : that.current != null) return false;
        if (newPassword != null ? !newPassword.equals(that.newPassword) : that.newPassword != null) return false;
        if (preEmail != null ? !preEmail.equals(that.preEmail) : that.preEmail != null) return false;
        if (preMobile != null ? !preMobile.equals(that.preMobile) : that.preMobile != null) return false;
        if (prePassword != null ? !prePassword.equals(that.prePassword) : that.prePassword != null) return false;
        if (prePhone != null ? !prePhone.equals(that.prePhone) : that.prePhone != null) return false;
        if (preUserEntity != null ? !preUserEntity.equals(that.preUserEntity) : that.preUserEntity != null)
            return false;
        if (preUsername != null ? !preUsername.equals(that.preUsername) : that.preUsername != null) return false;
        if (userInfo != null ? !userInfo.equals(that.userInfo) : that.userInfo != null) return false;
        if (userService != null ? !userService.equals(that.userService) : that.userService != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = curPassword != null ? curPassword.hashCode() : 0;
        result = 31 * result + (newPassword != null ? newPassword.hashCode() : 0);
        result = 31 * result + (preUsername != null ? preUsername.hashCode() : 0);
        result = 31 * result + (prePassword != null ? prePassword.hashCode() : 0);
        result = 31 * result + (preEmail != null ? preEmail.hashCode() : 0);
        result = 31 * result + (prePhone != null ? prePhone.hashCode() : 0);
        result = 31 * result + (preMobile != null ? preMobile.hashCode() : 0);
        result = 31 * result + (approved != null ? approved.hashCode() : 0);
        result = 31 * result + (userInfo != null ? userInfo.hashCode() : 0);
        result = 31 * result + (userService != null ? userService.hashCode() : 0);
        result = 31 * result + (preUserEntity != null ? preUserEntity.hashCode() : 0);
        result = 31 * result + (current != null ? current.hashCode() : 0);
        result = 31 * result + (canApprove != null ? canApprove.hashCode() : 0);
        return result;
    }
}
