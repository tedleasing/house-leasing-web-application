package com.uoadi.users.common.user.view;

import com.uoadi.users.common.user.entities.UserEntity;
import com.uoadi.users.common.user.helper.PasswordHashingHelper;
import com.uoadi.users.common.user.helper.UsersElements;
import com.uoadi.users.common.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

@ManagedBean
@ViewScoped
public class UserRegisterView implements Serializable{
    private static final long serialVersionUID = -8481377211230057402L;
    static final Logger LOG = LoggerFactory.getLogger(UserRegisterView.class);

    @EJB
    private UserService userService;
    private UsersElements userInfo;

    @PostConstruct
    public void init() {
        userInfo = new UsersElements();
        // Try to retrieve the role RequestParam
        String role = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("role");
        if (role != null) {
            // "Check" the specific checkbox
            if (role.equals(UserStateBean.SALESMAN))
                userInfo.setIsSalesman(true);
            else if (role.equals(UserStateBean.LESSEE))
                userInfo.setIsLessee(true);
            else if (role.equals(UserStateBean.LESSOR))
                userInfo.setIsLessor(true);
            else
                userInfo.setIsBuyer(true);
        }
        userService = new UserService();
        LOG.info("Created userService");
    }

    public String register() {
        Boolean result, con=true, privacy = true;
        UserEntity user = userInfo.getUser();
        String username = user.getUsername();
        String email = userInfo.getCommunication().geteMail();
        String mobile = userInfo.getCommunication().getMobile();
        String telephone = userInfo.getCommunication().getTelephone();
        String password = user.getPassword();
        UserEntity exist = userService.find(username, null, false);
        if(exist != null) {
            exist = null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το όνομα χρήστη "  + username + "υπάρχει ήδη, δοκιμάστε ένα άλλο", ""));
            con = false;
        }
        String message = " υπάρχει ήδη, παρακαλώ ελέξτε το πεδίο ξανά.\n";
        String privacyMessage = "Σε περίπτωση που πιστεύετε ότι κάποιος" +
                " χρησημοποιεί προσωπικά σας στοιχεία παρακαλούμε πολυ επικοινωνήστε άμεσα με το διαχειριστή!";
        result = userService.findCommunication(email, 1);
        if(result) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το email " + email + message, ""));
            privacy = false;
        }
        if(!password.equals(userInfo.getConfirmPassword())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Ο κωδικός με το πεδίο της επιβεβαίωσης του κωδικου δεν είναι ίδια", ""));
            con = false;
        }
        if(mobile.equals("___-___-____"))
            userInfo.getCommunication().setMobile("");
        result = userService.findCommunication(mobile, 2);
        if(result) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το κινητό " + mobile + message, ""));
            privacy = false;
        }
        if(telephone.equals("___-___-____"))
            userInfo.getCommunication().setTelephone("");
        result = userService.findCommunication(telephone, 3);
        if(result) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το σταθερό τηλέφωνο" + telephone + message, ""));
            privacy = false;
        }
        if(!privacy) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    privacyMessage, ""));
            return null;
        }
        if(!con)
            return null;
        if (!PasswordHashingHelper.hashPassword(userInfo.getUser())) {
            LOG.error("Hashing password failed");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Υπήρξε κάποιο σφάλμα κατά την εγγραφή σας, παρακαλώ προσπαθήστε ξανά", ""));
            return null;
        }
        userInfo.prepareNameDAO();
        userInfo.prepareCommunicationDAO();
        userInfo.prepareUserDAO();
        if( userService.insert(userInfo.getUser(), userInfo.getName(), userInfo.getCommunication()) != null)
            return "/successful_register" + "?faces-redirect=true";
        return "/failed_register" + "?faces-redirect=true";
    }

    public UsersElements getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UsersElements userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRegisterView that = (UserRegisterView) o;

        if (userInfo != null ? !userInfo.equals(that.userInfo) : that.userInfo != null) return false;
        if (userService != null ? !userService.equals(that.userService) : that.userService != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userService != null ? userService.hashCode() : 0;
        result = 31 * result + (userInfo != null ? userInfo.hashCode() : 0);
        return result;
    }
}
