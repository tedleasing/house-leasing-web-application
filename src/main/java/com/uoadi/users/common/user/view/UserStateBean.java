package com.uoadi.users.common.user.view;

import com.uoadi.users.common.user.helper.PasswordHashingHelper;
import com.uoadi.users.common.user.service.UserService;
import com.uoadi.users.common.user.entities.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ManagedBean
@SessionScoped
public class UserStateBean implements Serializable {
    private static final long serialVersionUID = -4560687049109606063L;
    public static final String BUYER = "buyer";
    public static final String SALESMAN = "salesman";
    public static final String LESSOR = "lessor";
    public static final String LESSEE = "lessee";
    private static final String ADMIN = "admin";


    static final Logger LOG = LoggerFactory.getLogger(UserStateBean.class);
    private String username;
    private String password;
    private UserEntity current;
    private Set<String> currentRoles;
    private String currentRole;
    @EJB
    private UserService userService;

    @PostConstruct
    public void init() {
        userService = new UserService();
        LOG.info("Created userService");
    }

    // -------------------------------------------------
    // UserService logic
    /**
     * Attempts to login the user by searching to the DB
     * If no user was found, an error is displayed
     * Otherwise, the user is redirected to a page
     * depending on their role in the application
     * @return The outcome string
     */
    public String login() {
        // First check that the user isn't already logged in
        if (isLoggedIn()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Είστε ήδη συνδεδεμένος/η!", ""));
            return null;
        }
        // *******************************************
        // Temporary code for hashing the current passwords
        // Set the upper bound to the number of users you want
        // to update their hash, set it to -1 for all users
//        List<UserEntity> users = userService.getAllUsers("foo");
//        int counter = 1, upper_bound = 7;
//        for (UserEntity user: users) {
//            PasswordHashingHelper.hashPassword(user);
//            if (!userService.updateUser(user, "none"))
//                LOG.debug("Something went wrong with user: " + user.getUsername());
//            if (counter > upper_bound && upper_bound != -1)
//                break;
//            counter++;
//        }
        // *******************************************
        current = userService.find(username, password, false);
        if (current == null ) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Το όνομα ή ο κωδικός χρήστη είναι λανθασμένα, προσπαθήστε ξανά", ""));
            return (username = password = null);
        }
        else {
            // update currentRoles
            updateRoles();
            if (currentRoles.size() > 1) {
                // More than one roles, user needs to be redirected to the role selection page
                // But first check if he is approved
                if (current.getApproved() == 1)
                    return "/user/select-role?faces-redirect=true";
                else
                    return "/successful_register?faces-redirect=true";
            }
            else if (currentRoles.size() == 1)
                currentRole = currentRoles.iterator().next();
            else
                return "/user/select-role?faces-redirect=true"; // Case in which the user has selected no roles
            // Found user, now where to redirect them
            if (current.getApproved() == 1)
                return userService.handleSingleRedirectCase(current);
            else
                return "/successful_register?faces-redirect=true";
        }
    }

    /**
     * Updates the current roles set
     * (Doesn't take into consideration the admin role)
     */
    public void updateRoles() {
        if (currentRoles == null)
            currentRoles = new HashSet<String>();
        if (isRole(BUYER))
            currentRoles.add(BUYER);
        if (isRole(SALESMAN))
            currentRoles.add(SALESMAN);
        if (isRole(LESSEE))
            currentRoles.add(LESSEE);
        if (isRole(LESSOR))
            currentRoles.add(LESSOR);
        if (isRole(ADMIN))
            currentRoles.add(ADMIN);
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index.xhtml?faces-redirect=true";
    }

    public void doLogout() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
        externalContext.redirect(externalContext.getRequestContextPath() + "/index.xhtml");
    }

    public boolean isLoggedIn() {
        return current != null;
    }

    public boolean isAdmin() { return userService.isAdminDoubleVerification(current);}

    public boolean profilePermissions() {
        return (current != null) || (userService.isAdminDoubleVerification(current));
    }

    /**
     * Return true if the user is of role "role"
     * @param role The role that the user will be checked for
     * @return True if the user's role is "role"
     */
    public boolean isRole(String role) {
        if (current == null)
            return false;
        else {
            if (role.equals(LESSOR))
                return current.getIsLeessor() == 1;
            else if (role.equals(LESSEE))
                return current.getIsLessee() == 1;
            else if (role.equals(BUYER))
                return current.getIsBuyer() == 1;
            else if (role.equals(SALESMAN))
                return current.getIsSalesman() == 1;
            else if (role.equals(ADMIN))
                return current.getIsAdmin() == 1;
            else
                return false;
        }
    }

    /**
     * Following are wrapper functions that use
     * the `isRole` method
     */
    public boolean isSalesman() { return isRole("salesman"); }
    public boolean isLessor() { return isRole("lessor"); }
    public boolean isLessee() { return isRole("lessee"); }
    public boolean isBuyer() { return isRole("buyer"); }


    /**
     * Sets the current role and redirects the user
     * @param role The role that the user selected
     * @return The page for the redirect
     */
    public String handleRedirect(String role) {
        currentRole = role;
        if (role.equals(UserStateBean.SALESMAN)) {
            if (current.getIsSalesman() == 1) {
                currentRole = "salesman";
                return "/offers/landing" + "?faces-redirect=true";
            }
            current.setIsSalesman((byte) 1);
        }
        else if (role.equals(UserStateBean.BUYER)) {
            if(current.getIsBuyer() == 1) {
                currentRole = "buyer";
                return "/demands/landing" + "?faces-redirect=true";
            }
            current.setIsBuyer((byte) 1);
        }
        else if (role.equals(UserStateBean.LESSEE)) {
            if(current.getIsLessee()==1) {
                currentRole = "lessee";
                return "/demands/landing" + "?faces-redirect=true";
            }
            current.setIsLessee((byte) 1);
        }
        else if (role.equals(UserStateBean.LESSOR)) {
            if(current.getIsLeessor()==1) {
                currentRole = "lessor";
                return "/offers/landing" + "?faces-redirect=true";
            }
            current.setIsLeessor((byte) 1);
        }
        else if (role.equals(UserStateBean.ADMIN)) {
            currentRole = "admin";
            return "/admin/home" + "?faces-redirect=true";
        }
        userService.updateUser(current, role);
        updateRoles();
        currentRole = "";
        return "/user/select-role?faces-redirect=true";
    }


    /**
     * Checks if the given role is the current one
     * @param role The role to be checked
     * @return True if the role given is the same as
     *          the current one, otherwise false
     */
    public boolean isCurrentRole(String role) {
        return role.equals(currentRole);
    }
    // -------------------------------------------------
    // Needed for Serializable
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserStateBean)) return false;

        UserStateBean that = (UserStateBean) o;

        if (current != null ? !current.equals(that.current) : that.current != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (!userService.equals(that.userService)) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (current != null ? current.hashCode() : 0);
        result = 31 * result + userService.hashCode();
        return result;
    }


    public void getMessages(ComponentSystemEvent event) {
        FacesMessage msg = (FacesMessage) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("message");
        if (msg != null) {
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void sendUserMessage(ActionEvent event) {
        for(UIComponent component : event.getComponent().getChildren()){
            if( component instanceof UIParameter){
                UIParameter param = (UIParameter) component;
                if(param.getName().equals("id")){
                    UserEntity userEntity = (UserEntity) param.getValue();
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
                            put("UserEntities", userEntity);
                }
            }
        }
    }
    // -------------------------------------------------
    // Getters/ Setters
    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserEntity getCurrent() {
        return current;
    }

    public void setCurrent(UserEntity current) {
        this.current = current;
    }
}