package com.uoadi.users.demands.filters;

import com.uoadi.users.common.user.view.UserStateBean;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/demands/*")
public class NotDemandsManAccessDemands implements Filter{

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        UserStateBean userState = (UserStateBean) session.getAttribute("userStateBean");
        // If the user is logged in but not a buyer or a lessee, redirect him to a
        // permission denied error page
        if(userState.isBuyer() && userState.isLessee() &&
                (!userState.isCurrentRole("buyer") && !userState.isCurrentRole("lessee")) )
            response.sendRedirect(request.getContextPath() + "/user/select-role.xhtml");
        else if (!userState.isBuyer() && !userState.isLessee())
            response.sendRedirect(request.getContextPath() + "/denied.xhtml");
        else
            filterChain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
