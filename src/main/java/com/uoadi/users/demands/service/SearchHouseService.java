package com.uoadi.users.demands.service;


import com.uoadi.users.common.house.dao.HouseDAOImpl;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.offers.helper.SearchHouse;
import com.uoadi.users.offers.helper.SearchResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class SearchHouseService {
    static final Logger LOG = LoggerFactory.getLogger(SearchHouseService.class);
    private static final long serialVersionUID = 3969434478839635415L;

    private HouseDAOImpl houseDAO;

    public SearchHouseService() {
        houseDAO = HouseDAOImpl.getHouseDAOImpl();
        LOG.info("Get HouseDAOImpl");
    }

    public List<SearchResults> search(SearchHouse searchHouse, Boolean isBuyer) {
        return houseDAO.searchHouse(searchHouse, isBuyer);
    }

    public SearchResults getHouseById(int houseId, Boolean isBuyer) {
        return houseDAO.searchHouseById(houseId, isBuyer);
    }

    /**
     * Wrapper method for persisting a message into the DB
     * @param msg The message to be persisted
     * @param senderID The userID of the sender
     *                 (used for the retrieval of their email)
     * @return True on success, otherwise false
     */
    public boolean persistMessage(MessagesEntity msg, int senderID) {
        return houseDAO.persistMessage(msg, senderID);
    }
}
