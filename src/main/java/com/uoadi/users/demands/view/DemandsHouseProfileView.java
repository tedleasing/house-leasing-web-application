package com.uoadi.users.demands.view;

import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;
import com.uoadi.users.common.user.service.UserService;
import com.uoadi.users.common.user.view.UserStateBean;
import com.uoadi.users.demands.service.SearchHouseService;
import com.uoadi.users.offers.helper.SearchPack;
import com.uoadi.users.offers.helper.SearchResults;
import org.omnifaces.util.Faces;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@ManagedBean
@ViewScoped
public class DemandsHouseProfileView implements Serializable{
    static final Logger LOG = LoggerFactory.getLogger(DemandsHouseProfileView.class);
    private static final long serialVersionUID = 3485511674965266772L;
    private static final String IMG_BASE = "/users/user";

    @EJB
    private SearchHouseService houseService;
    @EJB
    private UserService userService;

    @ManagedProperty("#{userStateBean}")
    private UserStateBean currentUser;

    // Data ------------------------------------------------------------------------------------------------------------
    private SearchResults info;
    private String imgPath;
    private String appDirPath;
    private MapModel mapModel;
    private MessagesEntity message;
    private int houseID;
    private SearchPack searchPack;

    @PostConstruct
    public void init(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String houseId = (String) facesContext.getExternalContext().
                getRequestParameterMap().get("id");
        houseID = Integer.parseInt(houseId);
        searchPack = Faces.getSessionAttribute("form");
        // Identify current user role and search
        // for the house accordingly
        boolean isBuyer;
        isBuyer = currentUser.getCurrentRole().equals(UserStateBean.BUYER);
        info = houseService.getHouseById(houseID, isBuyer);
        if (info != null) {
            // Setting image base path
            appDirPath = System.getProperty("user.home") + "/.harachi";
            imgPath = IMG_BASE + info.getUserEntity().getIdUser() + "/house"
                    + info.getHouseEntity().getIdHouse() + "/";
            // House mapModel
            mapModel = new DefaultMapModel();
            // Set latitude and longitude
            LocationEntity locationEntity = info.getHouseEntity().getLocationByIdLocation();
            Double lat = locationEntity.getLatitude();
            Double lng = locationEntity.getLongitude();
            if(lat != null && lng != null) {
                LatLng coords = new LatLng(lat, lng);
                mapModel.addOverlay(new Marker(coords, "House location"));
            }
            // Init message entity
            message = new MessagesEntity();
            LOG.info(currentUser.getCurrentRole() + " views house [" + houseID + "]");
        }
        else {
            // Prompt the user for erroneous house id
        }
    }

    public String back() {
        Faces.setSessionAttribute("form", searchPack);
        return "/demands/landing?faces-redirect=true";
    }


    public void sendMessage() {
        // Set houseID
        message.setIdHouse(houseID);
        // Set time
        message.setTime(new Timestamp(new Date().getTime()));
        // Save message
        boolean isSuccessful = houseService.persistMessage(message, currentUser.getCurrent().getIdUser());
        // Inform user of the outcome
        if (isSuccessful) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Το μήνυμα σας εστάλη επιτυχώς!", "");
            FacesContext.getCurrentInstance().addMessage("messages-send-messages", facesMessage);
            message.setTitle("");
            message.setMessage("");
        }
        else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Υπήρξε κάποιο πρόβλημα με την αποστολή " +
                    "του μηνύματος σας, παρακαλώ προσπαθήστε ξανά", "");
            FacesContext.getCurrentInstance().addMessage("messages-send-messages", message);
        }
    }


    // Getters and Setters ---------------------------------------------------------------------------------------------
    public MessagesEntity getMessage() {
        return message;
    }

    public void setMessage(MessagesEntity message) {
        this.message = message;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public String getImgPath() {
        return imgPath;
    }

    public String getAppDirPath() {
        return appDirPath;
    }

    public SearchResults getInfo() {
        return info;
    }

    public void setInfo(SearchResults info) {
        this.info = info;
    }

    public UserStateBean getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserStateBean currentUser) {
        this.currentUser = currentUser;
    }

    public SearchPack getSearchPack() {
        return searchPack;
    }

    public void setSearchPack(SearchPack searchPack) {
        this.searchPack = searchPack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DemandsHouseProfileView that = (DemandsHouseProfileView) o;

        if (houseID != that.houseID) return false;
        if (appDirPath != null ? !appDirPath.equals(that.appDirPath) : that.appDirPath != null) return false;
        if (currentUser != null ? !currentUser.equals(that.currentUser) : that.currentUser != null) return false;
        if (houseService != null ? !houseService.equals(that.houseService) : that.houseService != null) return false;
        if (imgPath != null ? !imgPath.equals(that.imgPath) : that.imgPath != null) return false;
        if (info != null ? !info.equals(that.info) : that.info != null) return false;
        if (mapModel != null ? !mapModel.equals(that.mapModel) : that.mapModel != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (searchPack != null ? !searchPack.equals(that.searchPack) : that.searchPack != null) return false;
        if (userService != null ? !userService.equals(that.userService) : that.userService != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = houseService != null ? houseService.hashCode() : 0;
        result = 31 * result + (userService != null ? userService.hashCode() : 0);
        result = 31 * result + (currentUser != null ? currentUser.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (imgPath != null ? imgPath.hashCode() : 0);
        result = 31 * result + (appDirPath != null ? appDirPath.hashCode() : 0);
        result = 31 * result + (mapModel != null ? mapModel.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + houseID;
        result = 31 * result + (searchPack != null ? searchPack.hashCode() : 0);
        return result;
    }
}
