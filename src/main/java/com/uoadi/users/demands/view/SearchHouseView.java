package com.uoadi.users.demands.view;

import com.uoadi.users.admin.service.AlgorithmsService;
import com.uoadi.users.common.algorithms.helper.AlgorithmHelper;
import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;
import com.uoadi.users.common.user.entities.UserEntity;
import com.uoadi.users.demands.service.SearchHouseService;
import com.uoadi.users.offers.helper.*;
import org.omnifaces.util.Faces;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import static java.lang.Math.*;

@ViewScoped
@ManagedBean
public class SearchHouseView implements Serializable {
    private static final long serialVersionUID = -8481377211230057402L;
    static final Logger LOG = LoggerFactory.getLogger(SearchHouseView.class);
    private static final String FLOOR_APT = "Στον ";
    private static final String FLOOR_HOUSE = "Αριθμός ορόφων";
    private static final String FLOOR_APT_END = " όροφο";

    @EJB
    private SearchHouseService houseService;
    @EJB
    private AlgorithmsService algorithmsService;

    private HouseEntity houseEntity;
    private String houseType;

    @ManagedProperty(value = "#{userStateBean.current}")
    private UserEntity user;
    @ManagedProperty(value = "#{userStateBean.currentRole}")
    private String currentRole;
    private String floorsLabel;
    private String floorsLabelEnd;
    private int curYear;

    private MapModel emptyModel;
    private List<SearchResults> searchResultsList;
    private List<AlgorithmHelper> availableAlgorithms;
    private SearchResults selectedHouse;
    private static int numOfResults = 10;
    private static int maxInt = Integer.MAX_VALUE;
    private List<String> algorithms;
    private SearchPack searchPack;
    List<SearchResults> result;

    public SearchHouseView() {
    }

    @PostConstruct
    public void init() {
        searchResultsList = new ArrayList<SearchResults>();
        houseEntity = new HouseEntity();
        result = new ArrayList<SearchResults>();
        emptyModel = new DefaultMapModel();
        floorsLabel = FLOOR_APT;
        floorsLabelEnd = FLOOR_APT_END;
        //get the current date
        curYear = Calendar.getInstance().get(Calendar.YEAR);
        houseType = "";
        searchPack = Faces.getSessionAttribute("form");
        if(searchPack == null)
            searchPack = new SearchPack();
        else
            search();
        availableAlgorithms = algorithmsService.getAbleAlgorithm((byte)1);
        algorithms = new ArrayList<String>();
        if(availableAlgorithms != null)
            for(AlgorithmHelper algo : availableAlgorithms) {
                algorithms.add(algo.getAlgorithmEntity().getDescription());
            }
        /*show houses in the map*/
        if((currentRole.equals("buyer") || currentRole.equals("lessee"))) {
            SearchHouse searchHouse = searchPack.getSearchHouse();
            Weight weight = searchPack.getWeight();
            searchHouse.setFromPrice(0);
            result = houseService.search(searchHouse, currentRole.equals("buyer"));
            for (SearchResults searchResults : result) {
                LocationEntity location = searchResults.getHouseEntity().getLocationByIdLocation();
                Double lat = location.getLatitude();
                Double lng = location.getLongitude();
                if (lat != null && lat != null) {
                    LatLng latLng = new LatLng(lat, lng);
                    emptyModel.addOverlay(new Marker(latLng, searchResults.getHouseEntity().getTitle()));
                }
            }
        }
    }

    public void search() {
        SearchHouse searchHouse = searchPack.getSearchHouse();
        Weight weight = searchPack.getWeight();
        String selectedAlgorithm = searchPack.getSelectedAlgorithm();
        result = houseService.search(searchHouse, currentRole.equals("buyer"));
        fixWeights();
        int extra = result.size();
        String url = "/demands/landing";
        String redirect = "?faces-redirect=true";
        if(selectedAlgorithm == null)
            searchResultsList = result;
        else if(selectedAlgorithm.toLowerCase().equals("topsis"))
            topsis((ArrayList<SearchResults>) result);
        else if(selectedAlgorithm.toUpperCase().equals("SAW"))
            saw((ArrayList<SearchResults>) result);
        for(SearchResults searchResults : searchResultsList) {
            LocationEntity location = searchResults.getHouseEntity().getLocationByIdLocation();
            Double lat = location.getLatitude();
            Double lng = location.getLongitude();
            if(lat != null && lat != null) {
                LatLng latLng = new LatLng(lat, lng);
                emptyModel.addOverlay(new Marker(latLng, searchResults.getHouseEntity().getTitle()));
            }
        }
        Faces.setSessionAttribute("form", searchPack);
    }

    /**
     * fix the weights that don't have values
     */
    private void fixWeights() {
        SearchHouse searchHouse = searchPack.getSearchHouse();
        Weight weight = searchPack.getWeight();
        if(searchHouse.getFromPrice()==null && searchHouse.getFromPrice()==null)
            weight.setPrice(0);
        if(searchHouse.getFormDateBuilt()==null && searchHouse.getToDateBuilt()==null)
            weight.setDateBuilt(0);
        if(searchHouse.getToSq()==null && searchHouse.getFromSq()==null)
            weight.setSq(0);
        if(searchHouse.getToDateRenovation()==null && searchHouse.getFromDateRenovation()==null)
            weight.setRenovationDate(0);
        if(searchHouse.getToPublicExpenditure()==null && searchHouse.getFromPublicExpenditure()==null)
            weight.setPublicExpenditure(0);
    }

    /**
     * redirect to house's profile
     * @return
     */
    public String viewThisHouse() {
        if (selectedHouse != null) {
            Faces.setSessionAttribute("form", searchPack);
            return "/demands/house.xhtml?id=" + selectedHouse.getHouseEntity().getIdHouse() + "&faces-redirect=true";
        }
        else
            return null;
    }

    /**
     * redirect to house's profile from map
     * @param event
     */
    public void onMarkerSelect(OverlaySelectEvent event) {
        Marker marker = (Marker) event.getOverlay();
        for(SearchResults searchResults : result) {
            LocationEntity location = searchResults.getHouseEntity().getLocationByIdLocation();
            Double lat = location.getLatitude();
            Double lng = location.getLongitude();
            LatLng latLng = marker.getLatlng();
            if(lat != null) {
                if (latLng.getLat() == lat && latLng.getLng() == lng) {
                    Faces.setSessionAttribute("form", searchPack);
                    HouseValuePack selectedHouse = new HouseValuePack(searchResults.getHouseEntity(),
                            searchResults.getValueEntity());
                    Faces.setSessionAttribute("selectedEditHouse", selectedHouse);
                    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                    try {
                        ec.redirect(ec.getRequestContextPath() + "/demands/house.xhtml?id=" + searchResults.getHouseEntity().getIdHouse());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
//        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Selected", marker.getTitle()));
    }

    public void houseTypeChanged()  {
        if (houseType.equals("Διαμέρισμα")) {
            floorsLabel = FLOOR_APT;
            floorsLabelEnd = FLOOR_APT_END;
        }
        else if(houseType.equals("Μονοκατοικία")) {
            floorsLabel = FLOOR_HOUSE;
            floorsLabelEnd = "";
        }
    }

    /*algorithms*/

    /**
     * fix the fields that don't have a value
     * fields with  positive importance (greater is better) get the value 0
     * fields with negative importance (lower is better) get the greatest value of int
     * so their values are ignored
     * @param searchResult
     * @return
     */
    private ArrayList<double[]> resetFields(ArrayList<SearchResults> searchResult) {
        ArrayList<double[]> valueInts = new ArrayList<double[]>();
        for(SearchResults result : searchResult) {
            double[] fields = new double[5];
            Integer value = result.getValueEntity().getPrice();
            fields[0] = value==null ? maxInt : value;
            HouseEntity house = result.getHouseEntity();
            value = house.getDateBuilt();
            fields[1] = value==null ? 0 : value;
            value = house.getRenovationDate();
            fields[2] = value==null ? 0 : value;
            value = house.getPublicExpenditure();
            fields[3] = value==null ? maxInt : value;
            value = house.getSq();
            fields[4] = value==null ? 0 : value;
            valueInts.add(fields);
        }
        return valueInts;
    }

    /**
     * calculate an array with the values multiply with their weight
     * @param valueInts
     * @return
     */
    private ArrayList<double[]> calculateValueWithWeights(ArrayList<double[]> valueInts) {
        Weight weight = searchPack.getWeight();
        for(double[] value : valueInts) {
            value[0] *= weight.getPrice();
            value[1] *= weight.getDateBuilt();
            value[2] *= weight.getRenovationDate();
            value[3] *= weight.getPublicExpenditure();
            value[4] *= weight.getSq();
        }
        return valueInts;
    }

    /**
     * sort the records which will be presented to the user and restrict their number
     * @param searchResult
     * @param cPos
     */
    private void prepareSearchResults(ArrayList<SearchResults> searchResult ,double[][] cPos) {
        Arrays.sort(cPos, new Comparator<double[]>() {
            public int compare(double[] a, double[] b) {
                return Double.compare(a[0], b[0]);
            }
        });
        searchResultsList = new ArrayList<SearchResults>();
        for(int i=cPos.length-1; i>=0 && i>cPos.length-numOfResults-1; i--) {
            searchResultsList.add(searchResult.get((int) cPos[i][1]));
        }
    }

    private void topsis(ArrayList<SearchResults> searchResult) {
        ArrayList<double[]> valueInts = new ArrayList<double[]>();
        valueInts = resetFields(searchResult);

        /*sum all the values which have a weight (5) for all the records*/
        double[] sum = new double[5];
        for(double[] value : valueInts) {
            for(int i=0; i<5; i++) {
                sum[i] += (int) pow(value[i],2);
            }
        }

        /*kanonikopoihsh twn timwn*/
        for(int j=0; j<valueInts.size(); j++) {
            double[] value = valueInts.get(j);
            for(int i=0; i<5; i++) {
                double sumSqrt = (double) (sum[i]);
                value[i] /= sumSqrt;
            }
        }

        //calculate the values with their weights
        valueInts = calculateValueWithWeights(valueInts);

        //calculate positive and negative arrays for the attributes of all the records
        double[] aPos= new double[5];
        double[] aNeg= new double[5];
        for(int i=0; i<5; i++) {
            aPos[i] = valueInts.get(0)[i];
            aNeg[i] = valueInts.get(0)[i];
        }
        for(int i=1; i<valueInts.size(); i++) {
            aPos[0] = min(aPos[0], valueInts.get(i)[0]);
            aPos[1] = max(aPos[1], valueInts.get(i)[1]);
            aPos[2] = max(aPos[2], valueInts.get(i)[2]);
            aPos[3] = min(aPos[3], valueInts.get(i)[3]);
            aPos[4] = max(aPos[4], valueInts.get(i)[4]);

            aNeg[0] = max(aNeg[0], valueInts.get(i)[0]);
            aNeg[1] = min(aNeg[1], valueInts.get(i)[1]);
            aNeg[2] = min(aNeg[2], valueInts.get(i)[2]);
            aNeg[3] = max(aNeg[3], valueInts.get(i)[3]);
            aNeg[4] = min(aNeg[4], valueInts.get(i)[4]);
        }

        double[] sPos = new double[valueInts.size()];
        double[] sNeg = new double[valueInts.size()];
        for(int j=0; j<valueInts.size(); j++) {
            sPos[j] = sNeg[j] = 0;
        }
        for(int i = 0; i<5; i++) {
            for(int j=0; j<valueInts.size(); j++) {
                sPos[j] += pow(aPos[i] - valueInts.get(j)[i],2);
                sNeg[j] += pow(aNeg[i] - valueInts.get(j)[i],2);
            }
        }
        for(int j=0; j<valueInts.size(); j++) {
            sPos[j] = sqrt(sPos[j]);
            sNeg[j] = sqrt(sNeg[j]);
        }

        double[][] cPos = new double[valueInts.size()][2];
        for(int i=0; i<cPos.length; i++) {
            cPos[i][0] = sNeg[i]/(sPos[i] + sNeg[i]);
            cPos[i][1] = i;
        }
        prepareSearchResults(searchResult, cPos);
    }

    /**
     * sum the attributes multiplied with their weight of all the records and present the greatest results
     * @param searchResult
     */
    private void saw(ArrayList<SearchResults> searchResult) {
        ArrayList<double[]> valueInts = calculateValueWithWeights(resetFields(searchResult));

        double[] sum = new double[valueInts.size()];
        for(int j=0; j<valueInts.size(); j++) {
            for(int i=0; i<5; i++) {
                sum[j] += valueInts.get(j)[i];
            }
        }

        double[][] cPos = new double[valueInts.size()][2];
        for(int i=0; i<cPos.length; i++) {
            cPos[i][0] = 1/sum[i];
            cPos[i][1] = i;
        }
        prepareSearchResults(searchResult, cPos);
    }


    /*Getter-Setter*/
    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        if(houseType == null)
            houseType = "";
        this.houseType = houseType;
        Byte value = null;
        if(houseType.equals("Διαμέρισμα")) {
            value = 1;
            houseEntity.setApartment(value);
        }
        else if(houseType.equals("Μονοκατοικία")) {
            value = 0;
            houseEntity.setApartment(value);
        }
        searchPack.getSearchHouse().setApartment(value);
    }

    public int getCurYear() {
        return curYear;
    }

    public void setCurYear(int curYear) {
        this.curYear = curYear;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public MapModel getEmptyModel() {
        return emptyModel;
    }

    public void setEmptyModel(MapModel emptyModel) {
        this.emptyModel = emptyModel;
    }

    public String getFloorsLabelEnd() {
        return floorsLabelEnd;
    }

    public void setFloorsLabelEnd(String floorsLabelEnd) {
        this.floorsLabelEnd = floorsLabelEnd;
    }

    public String getFloorsLabel() {
        return floorsLabel;
    }

    public void setFloorsLabel(String floorsLabel) {
        this.floorsLabel = floorsLabel;
    }

    public List<SearchResults> getSearchResultsList() {
        return searchResultsList;
    }

    public void setSearchResultsList(List<SearchResults> searchResultsList) {
        this.searchResultsList = searchResultsList;
    }

    public SearchResults getSelectedHouse() {
        return selectedHouse;
    }

    public void setSelectedHouse(SearchResults selectedHouse) {
        this.selectedHouse = selectedHouse;
    }

    public List<String> getAlgorithms() {
        return algorithms;
    }

    public void setAlgorithms(List<String> algorithms) {
        this.algorithms = algorithms;
    }

    public SearchPack getSearchPack() {
        return searchPack;
    }

    public void setSearchPack(SearchPack searchPack) {
        this.searchPack = searchPack;
    }

    /*equal/hashCode*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchHouseView that = (SearchHouseView) o;

        if (curYear != that.curYear) return false;
        if (algorithms != null ? !algorithms.equals(that.algorithms) : that.algorithms != null) return false;
        if (algorithmsService != null ? !algorithmsService.equals(that.algorithmsService) : that.algorithmsService != null)
            return false;
        if (availableAlgorithms != null ? !availableAlgorithms.equals(that.availableAlgorithms) : that.availableAlgorithms != null)
            return false;
        if (currentRole != null ? !currentRole.equals(that.currentRole) : that.currentRole != null) return false;
        if (emptyModel != null ? !emptyModel.equals(that.emptyModel) : that.emptyModel != null) return false;
        if (floorsLabel != null ? !floorsLabel.equals(that.floorsLabel) : that.floorsLabel != null) return false;
        if (floorsLabelEnd != null ? !floorsLabelEnd.equals(that.floorsLabelEnd) : that.floorsLabelEnd != null)
            return false;
        if (houseEntity != null ? !houseEntity.equals(that.houseEntity) : that.houseEntity != null) return false;
        if (houseService != null ? !houseService.equals(that.houseService) : that.houseService != null) return false;
        if (houseType != null ? !houseType.equals(that.houseType) : that.houseType != null) return false;
        if (result != null ? !result.equals(that.result) : that.result != null) return false;
        if (searchPack != null ? !searchPack.equals(that.searchPack) : that.searchPack != null) return false;
        if (searchResultsList != null ? !searchResultsList.equals(that.searchResultsList) : that.searchResultsList != null)
            return false;
        if (selectedHouse != null ? !selectedHouse.equals(that.selectedHouse) : that.selectedHouse != null)
            return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result1 = houseService != null ? houseService.hashCode() : 0;
        result1 = 31 * result1 + (algorithmsService != null ? algorithmsService.hashCode() : 0);
        result1 = 31 * result1 + (houseEntity != null ? houseEntity.hashCode() : 0);
        result1 = 31 * result1 + (houseType != null ? houseType.hashCode() : 0);
        result1 = 31 * result1 + (user != null ? user.hashCode() : 0);
        result1 = 31 * result1 + (currentRole != null ? currentRole.hashCode() : 0);
        result1 = 31 * result1 + (floorsLabel != null ? floorsLabel.hashCode() : 0);
        result1 = 31 * result1 + (floorsLabelEnd != null ? floorsLabelEnd.hashCode() : 0);
        result1 = 31 * result1 + curYear;
        result1 = 31 * result1 + (emptyModel != null ? emptyModel.hashCode() : 0);
        result1 = 31 * result1 + (searchResultsList != null ? searchResultsList.hashCode() : 0);
        result1 = 31 * result1 + (availableAlgorithms != null ? availableAlgorithms.hashCode() : 0);
        result1 = 31 * result1 + (selectedHouse != null ? selectedHouse.hashCode() : 0);
        result1 = 31 * result1 + (algorithms != null ? algorithms.hashCode() : 0);
        result1 = 31 * result1 + (searchPack != null ? searchPack.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        return result1;
    }
}
