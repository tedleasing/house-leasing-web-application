package com.uoadi.users.offers.entities;

import com.uoadi.users.common.user.entities.UserEntity;

import javax.persistence.*;

@Entity
@Table(name = "salesman", schema = "", catalog = "myhouse")
public class SalesmanEntity {
    private int idUser;
    private UserEntity userByIdUser;

    @Id
    @Column(name = "idUser", nullable = false, insertable = true, updatable = true)
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SalesmanEntity that = (SalesmanEntity) o;

        if (idUser != that.idUser) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return idUser;
    }

    @OneToOne
    @JoinColumn(name = "idUser", referencedColumnName = "idUser", nullable = false)
    public UserEntity getUserByIdUser() {
        return userByIdUser;
    }

    public void setUserByIdUser(UserEntity userByIdUser) {
        this.userByIdUser = userByIdUser;
    }
}
