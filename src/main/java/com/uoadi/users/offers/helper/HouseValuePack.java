package com.uoadi.users.offers.helper;


import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house.entities.info.ImagesEntity;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.common.house.entities.location.AddressEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;
import com.uoadi.users.common.house_transaction.entities.ValueEntity;
import org.hibernate.LazyInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 * Acts as a wrapper class containing:
 *  - a HouseEntity
 *  - a ValueEntity
 */
public class HouseValuePack implements Serializable {
    private static final long serialVersionUID = -2269529525479648653L;
    static final Logger LOG = LoggerFactory.getLogger(HouseValuePack.class);
    HouseEntity house;
    ValueEntity value;

    public HouseValuePack(HouseEntity house, ValueEntity value) {
        this.house = house;
        this.value = value;
    }

    public HouseValuePack(HouseValuePack hvp) {
        // Copying HouseEntity
        this.house = new HouseEntity();
        this.house.setApartment(hvp.getHouse().getApartment());
        this.house.setDateBuilt(hvp.getHouse().getDateBuilt());
        this.house.setRenovationDate(hvp.getHouse().getRenovationDate());
        this.house.setFloor(hvp.getHouse().getFloor());
        this.house.setHeating(hvp.getHouse().getHeating());
        this.house.setIdHouse(hvp.getHouse().getIdHouse());
        this.house.setSq(hvp.getHouse().getSq());
        this.house.setPublicExpenditure(hvp.getHouse().getPublicExpenditure());
        if (hvp.getHouse().getImagesByIdHouse() == null)
            this.house.setImagesByIdHouse(null);
        else
            copyImages(hvp.getHouse().getImagesByIdHouse());
        if (hvp.getHouse().getLocationByIdLocation() == null)
            this.house.setLocationByIdLocation(null);
        else
            copyLocation(hvp.getHouse().getLocationByIdLocation());
        if (hvp.getHouse().getMessagesByIdHouse() == null)
            this.house.setMessagesByIdHouse(null);
        else
            this.house.setMessagesByIdHouse(hvp.getHouse().getMessagesByIdHouse());
        // Copying ValueEntity
        this.value = new ValueEntity();
        this.value.setIdValue(hvp.getValue().getIdValue());
        this.value.setPrice(hvp.getValue().getPrice());
        this.value.setNegotiable(hvp.getValue().getNegotiable());
    }

    // Copy constructor helper methods
    private void copyImages(Collection<ImagesEntity> images){
        if(images == null)
            return;
        Collection<ImagesEntity> copy = new HashSet<ImagesEntity>();
        try {
            for (ImagesEntity img : images) {
                ImagesEntity copyImg = new ImagesEntity();
                copyImg.setDescription(img.getDescription());
                copyImg.setIdImage(img.getIdImage());
                copyImg.setUrl(img.getUrl());
                if (img.getHouseByIdHouse() == null)
                    copyImg.setHouseByIdHouse(null);
                else
                    copyImg.setHouseByIdHouse(this.house);
                copy.add(copyImg);
            }
            this.house.setImagesByIdHouse(copy);
        }
        catch (LazyInitializationException e) {
            LOG.debug("HouseValuePack/copyImages: No image found!");
            return;
        }
    }

    private void copyMessages(Collection<MessagesEntity> messages) {
        Collection<MessagesEntity> copy = new HashSet<MessagesEntity>();
        for(MessagesEntity msg: messages) {
            MessagesEntity copyMsg = new MessagesEntity();
            copyMsg.setIdMessage(msg.getIdMessage());
            copyMsg.setEmail(msg.getEmail());
            copyMsg.setMessage(msg.getMessage());
            copyMsg.setTime(msg.getTime());
            copyMsg.setTitle(msg.getTitle());
            copy.add(copyMsg);
        }
        this.house.setMessagesByIdHouse(copy);
    }

    private void copyLocation(LocationEntity location) {
        LocationEntity copyLoc = new LocationEntity();
        copyLoc.setIdLocation(location.getIdLocation());
        copyLoc.setCountry(location.getCountry());
        copyLoc.setState(location.getState());
        copyLoc.setCity(location.getCity());
        copyLoc.setLatitude(location.getLatitude());
        copyLoc.setLongitude(location.getLongitude());
        if (location.getAddressByIdAddress() == null)
            copyLoc.setAddressByIdAddress(null);
        else
            copyLoc.setAddressByIdAddress(copyAddress(location.getAddressByIdAddress()));
        this.house.setLocationByIdLocation(copyLoc);
    }

    private AddressEntity copyAddress(AddressEntity address) {
        AddressEntity copyAddr = new AddressEntity();
        copyAddr.setIdAddress(address.getIdAddress());
        copyAddr.setHouseNumber(address.getHouseNumber());
        copyAddr.setRoadName(address.getRoadName());
        return copyAddr;
    }

    // Getters and Setters --------------------------------------------------------------------------------------------
    public HouseEntity getHouse() {
        return house;
    }

    public void setHouse(HouseEntity house) {
        this.house = house;
    }

    public ValueEntity getValue() {
        return value;
    }

    public void setValue(ValueEntity value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HouseValuePack)) return false;

        HouseValuePack that = (HouseValuePack) o;

        if (house != null ? !house.equals(that.house) : that.house != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = house != null ? house.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
