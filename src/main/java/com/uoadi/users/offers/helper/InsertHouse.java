package com.uoadi.users.offers.helper;

import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house.entities.location.AddressEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasLessorEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasSalesmanEntity;
import com.uoadi.users.common.house_transaction.entities.ValueEntity;


public class InsertHouse {
    private HouseEntity houseEntity;
    private AddressEntity addressEntity;
    private LocationEntity locationEntity;
    private ValueEntity valueEntity;
    private HouseHasLessorEntity houseHasLessorEntity;
    private HouseHasSalesmanEntity houseHasSalesmanEntity;

    public InsertHouse(int userId) {
        houseEntity = new HouseEntity();
        addressEntity = new AddressEntity();
        locationEntity = new LocationEntity();
        locationEntity.setCountry("Ελλάδα");
        valueEntity = new ValueEntity();
        houseHasLessorEntity = new HouseHasLessorEntity();
        houseHasSalesmanEntity = new HouseHasSalesmanEntity();
        houseHasLessorEntity.setIdUser(userId);
        houseHasSalesmanEntity.setIdUser(userId);
    }

    public HouseEntity getHouseEntity() {
        return houseEntity;
    }

    public void setHouseEntity(HouseEntity houseEntity) {
        this.houseEntity = houseEntity;
    }

    public AddressEntity getAddressEntity() {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity) {
        this.addressEntity = addressEntity;
    }

    public LocationEntity getLocationEntity() {
        return locationEntity;
    }

    public void setLocationEntity(LocationEntity locationEntity) {
        this.locationEntity = locationEntity;
    }

    public ValueEntity getValueEntity() {
        return valueEntity;
    }

    public void setValueEntity(ValueEntity valueEntity) {
        this.valueEntity = valueEntity;
    }

    public HouseHasLessorEntity getHouseHasLessorEntity() {
        return houseHasLessorEntity;
    }

    public void setHouseHasLessorEntity(HouseHasLessorEntity houseHasLessorEntity) {
        this.houseHasLessorEntity = houseHasLessorEntity;
    }

    public HouseHasSalesmanEntity getHouseHasSalesmanEntity() {
        return houseHasSalesmanEntity;
    }

    public void setHouseHasSalesmanEntity(HouseHasSalesmanEntity houseHasSalesmanEntity) {
        this.houseHasSalesmanEntity = houseHasSalesmanEntity;
    }
}
