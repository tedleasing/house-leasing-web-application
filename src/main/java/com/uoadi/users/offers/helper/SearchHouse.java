package com.uoadi.users.offers.helper;


import com.uoadi.users.common.house.entities.location.AddressEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;

public class SearchHouse {
    private Integer fromPrice;
    private Integer toPrice;
    private Integer formDateBuilt;
    private Integer toDateBuilt;
    private Integer fromSq;
    private Integer toSq;
    private Integer fromDateRenovation;
    private Integer toDateRenovation;
    private Integer fromPublicExpenditure;
    private Integer toPublicExpenditure;
    private String heating;
    private Integer floor;
    private Byte apartment;
    private Byte negotiable;
    private Boolean negotiableABoolean;
    private LocationEntity locationEntity;
    private AddressEntity addressEntity;

    public SearchHouse() {
        locationEntity = new LocationEntity();
        addressEntity = new AddressEntity();
        heating = "";
        locationEntity.setCity("");
        locationEntity.setState("");
        locationEntity.setCountry("Ελλάδα");
        addressEntity.setHouseNumber("");
        addressEntity.setRoadName("");
        fromDateRenovation = null;
        fromPrice = null;
        fromPublicExpenditure = null;
        fromSq = null;
        floor = null;
        apartment = null;
        negotiable = null;
        toDateBuilt = null;
        toDateRenovation = null;
        toPrice = null;
        toPublicExpenditure = null;
        toSq = null;
    }

    public LocationEntity getLocationEntity() {
        return locationEntity;
    }

    public void setLocationEntity(LocationEntity locationEntity) {
        this.locationEntity = locationEntity;
    }

    public AddressEntity getAddressEntity() {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity) {
        this.addressEntity = addressEntity;
    }

    public Boolean getNegotiable() {
        if(negotiable == 1)
            return true;
        return false;
    }

    public Boolean getNegotiableABoolean() {
        return negotiableABoolean;
    }

    public void setNegotiableABoolean(Boolean negotiableABoolean) {
        this.negotiableABoolean = negotiableABoolean;
    }

    public void setNegotiable(Boolean negotiablePrice) {
        negotiableABoolean = negotiablePrice;
        Byte value = 0;
        if(negotiablePrice)
            value = 1;
        this.negotiable = value;
    }

    public String getHeating() {
        return heating;
    }

    public void setHeating(String heating) {
        this.heating = heating;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getFromPrice() {
        return fromPrice;
    }

    public void setFromPrice(Integer fromPrice) {
        this.fromPrice = fromPrice;
    }

    public Integer getToPrice() {
        return toPrice;
    }

    public void setToPrice(Integer toPrice) {
        this.toPrice = toPrice;
    }

    public Integer getFormDateBuilt() {
        return formDateBuilt;
    }

    public void setFormDateBuilt(Integer formDateBuilt) {
        this.formDateBuilt = formDateBuilt;
    }

    public Integer getToDateBuilt() {
        return toDateBuilt;
    }

    public void setToDateBuilt(Integer toDateBuilt) {
        this.toDateBuilt = toDateBuilt;
    }

    public Integer getFromSq() {
        return fromSq;
    }

    public void setFromSq(Integer fromSq) {
        this.fromSq = fromSq;
    }

    public Integer getToSq() {
        return toSq;
    }

    public void setToSq(Integer toSq) {
        this.toSq = toSq;
    }

    public Byte getApartment() {
        return apartment;
    }

    public void setApartment(Byte apartment) {
        this.apartment = apartment;
    }

    public Integer getFromDateRenovation() {
        return fromDateRenovation;
    }

    public void setFromDateRenovation(Integer fromDateRenovation) {
        this.fromDateRenovation = fromDateRenovation;
    }

    public Integer getToDateRenovation() {
        return toDateRenovation;
    }

    public void setToDateRenovation(Integer toDateRenovation) {
        this.toDateRenovation = toDateRenovation;
    }

    public Integer getFromPublicExpenditure() {
        return fromPublicExpenditure;
    }

    public void setFromPublicExpenditure(Integer fromPublicExpenditure) {
        this.fromPublicExpenditure = fromPublicExpenditure;
    }

    public Integer getToPublicExpenditure() {
        return toPublicExpenditure;
    }

    public void setToPublicExpenditure(Integer toPublicExpenditure) {
        this.toPublicExpenditure = toPublicExpenditure;
    }
}
