package com.uoadi.users.offers.helper;

import java.io.Serializable;

public class SearchPack implements Serializable{
    private static final long serialVersionUID = -2966550362197136279L;
    private SearchHouse searchHouse;
    private Weight weight;
    private String selectedAlgorithm;

    public SearchPack() {
        searchHouse= new SearchHouse();
        weight = new Weight();
    }

    public SearchHouse getSearchHouse() {
        return searchHouse;
    }

    public void setSearchHouse(SearchHouse searchHouse) {
        this.searchHouse = searchHouse;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public String getSelectedAlgorithm() {
        return selectedAlgorithm;
    }

    public void setSelectedAlgorithm(String selectedAlgorithm) {
        this.selectedAlgorithm = selectedAlgorithm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchPack)) return false;

        SearchPack that = (SearchPack) o;

        if (searchHouse != null ? !searchHouse.equals(that.searchHouse) : that.searchHouse != null) return false;
        if (selectedAlgorithm != null ? !selectedAlgorithm.equals(that.selectedAlgorithm) : that.selectedAlgorithm != null)
            return false;
        if (weight != null ? !weight.equals(that.weight) : that.weight != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = searchHouse != null ? searchHouse.hashCode() : 0;
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (selectedAlgorithm != null ? selectedAlgorithm.hashCode() : 0);
        return result;
    }
}
