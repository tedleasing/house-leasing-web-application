package com.uoadi.users.offers.helper;


import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house_transaction.entities.ValueEntity;
import com.uoadi.users.common.user.entities.UserEntity;

public class SearchResults {
    HouseEntity houseEntity;
    ValueEntity valueEntity;
    UserEntity userEntity;

    public SearchResults() {
        houseEntity = new HouseEntity();
        valueEntity = new ValueEntity();
        userEntity = new UserEntity();
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public HouseEntity getHouseEntity() {
        return houseEntity;
    }

    public void setHouseEntity(HouseEntity houseEntity) {
        this.houseEntity = houseEntity;
    }

    public ValueEntity getValueEntity() {
        return valueEntity;
    }

    public void setValueEntity(ValueEntity valueEntity) {
        this.valueEntity = valueEntity;
    }
}
