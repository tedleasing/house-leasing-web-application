package com.uoadi.users.offers.helper;


public class Weight {
    private Integer dateBuilt;
    private Integer renovationDate;
    private Integer publicExpenditure;
    private Integer heating;
    private Integer sq;
    private Integer apartment;
    private Integer floor;
    private Integer state;
    private Integer city;
    private Integer latitude;
    private Integer longitude;
    private Integer roadName;
    private Integer houseNumber;
    private Integer price;
    private Integer negotiable;

    public Weight() {
        int defaultValue = 1;
        dateBuilt = defaultValue;
        renovationDate = defaultValue;
        publicExpenditure = defaultValue;
        heating = defaultValue;
        sq = defaultValue;
        apartment = defaultValue;
        floor = defaultValue;
        state = defaultValue;
        city = defaultValue;
        latitude = defaultValue;
        longitude = defaultValue;
        roadName = defaultValue;
        houseNumber = defaultValue;
        price = defaultValue;
        negotiable = defaultValue;
    }

    public Integer getDateBuilt() {
        return dateBuilt;
    }

    public void setDateBuilt(Integer dateBuilt) {
        this.dateBuilt = dateBuilt;
    }

    public Integer getRenovationDate() {
        return renovationDate;
    }

    public void setRenovationDate(Integer renovationDate) {
        this.renovationDate = renovationDate;
    }

    public Integer getPublicExpenditure() {
        return publicExpenditure;
    }

    public void setPublicExpenditure(Integer publicExpenditure) {
        this.publicExpenditure = publicExpenditure;
    }

    public Integer getSq() {
        return sq;
    }

    public void setSq(Integer sq) {
        this.sq = sq;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getHeating() {
        return heating;
    }

    public void setHeating(Integer heating) {
        this.heating = heating;
    }

    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer apartment) {
        this.apartment = apartment;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    public Integer getRoadName() {
        return roadName;
    }

    public void setRoadName(Integer roadName) {
        this.roadName = roadName;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(Integer negotiable) {
        this.negotiable = negotiable;
    }
}
