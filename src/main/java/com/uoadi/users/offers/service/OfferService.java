package com.uoadi.users.offers.service;

import com.uoadi.users.common.house.dao.HouseDAOImpl;
import com.uoadi.users.common.house.entities.info.ImagesEntity;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.common.house_transaction.dao.HouseTransactionsDAOImpl;
import com.uoadi.users.offers.helper.HouseValuePack;
import com.uoadi.users.offers.helper.InsertHouse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

@Stateless
public class OfferService implements Serializable{
    static final Logger LOG = LoggerFactory.getLogger(OfferService.class);
    private static final long serialVersionUID = -7036776094447885715L;

    // Outcome messages -----------------------------------------------------------------------------------------------
    public static final String RESULT_HV_VALUE_UPDATED = "Οι αλλαγές που αφορούν την τιμή αποθηκεύθηκαν," +
            "όμως οι αλλαγές της οικίας όχι. Παρακαλώ προσπαθήστε ξανά για την ανανέωση αυτών";
    public static final String RESULT_HV_HOUSE_UPDATED = "Οι αλλαγές που αφορούν το σπίτι αποθηκεύθηκαν," +
            "όμως οι αλλαγές της τιμή όχι. Παρακαλώ προσπαθήστε ξανά για την ανανέωση αυτών";
    public static final String RESULT_HV_HOUSE_VALUE_UPDATED = "Οι αλλαγές σας αποθηκεύθηκαν επιτυχώς!";
    public static final String RESULT_HV_NONE_UPDATED = "Υπήρξε κάποιο πρόβλημα με την αποθήκευση των αλλαγών" +
            " σας, παρακαλώ προσπαθήστε ξανά!";

    // Data -----------------------------------------------------------------------------------------------------------
    private HouseTransactionsDAOImpl offerTransactionsDAO;
    private HouseDAOImpl houseDAO;

    public OfferService() {
        offerTransactionsDAO = HouseTransactionsDAOImpl.getHouseTransactionDAO();
        houseDAO = HouseDAOImpl.getHouseDAOImpl();
    }

    public List<HouseValuePack> getOfferedHouses(Integer userID, boolean isSalesman) {
        return offerTransactionsDAO.housesOfferedFromUser(userID, isSalesman);
    }


    /**
     * Updates the house and value entities in the DB
     * If something went wrong,
     * @param houseValuePack The house and value entities pack
     *                       to be updated
     * @return A descriptive message for the user regarding the
     *          outcome
     */
    public String updateHouseProfile(HouseValuePack houseValuePack) {
        boolean valueUpdate = offerTransactionsDAO.updateHouseValue(houseValuePack.getValue());
        boolean houseUpdate = houseDAO.updateHouse(houseValuePack.getHouse());
        if (valueUpdate && houseUpdate)
            return RESULT_HV_HOUSE_VALUE_UPDATED;
        else if (valueUpdate && !houseUpdate)
            return RESULT_HV_VALUE_UPDATED;
        else if (!valueUpdate && houseUpdate)
            return RESULT_HV_HOUSE_UPDATED;
        else
            return RESULT_HV_NONE_UPDATED;
    }


    /**
     * Wrapper function for getting all the images for a house
     * @param houseID The houseID of which the images are to
     *                be retrieved
     * @return The list of the images
     */
    public List<ImagesEntity> getImages(Integer houseID) {
        return houseDAO.getAllImages(houseID);
    }


    /**
     * Wrapper function for deleting an Image from the DB
     * @param img The image entity to be deleted
     * @return True on success, otherwise false
     */
    public boolean deleteImage(ImagesEntity img) {
        return houseDAO.deleteImage(img);
    }

    /**
     * Wrapper function for persisting an Image into the DB
     * @param img The image entity to be inserted
     * @return True on success, otherwise false
     */
    public boolean persistImage(ImagesEntity img, boolean isNew) {
        return houseDAO.persistImage(img, isNew);
    }

    public Integer insertHouse(InsertHouse house) {

        return offerTransactionsDAO.insertHouse(house);
    }

    /**
     * Acts as a wrapper for the deleteHouse of the DAO
     * @param houseValuePack The house to be deleted
     * @param isFromSalesman If the house is listed from a salesman
     *                       or a lessor
     * @return True on success, false otherwise
     */
    public boolean deleteHouse(HouseValuePack houseValuePack, boolean isFromSalesman) {
        return offerTransactionsDAO.deleteHouse(houseValuePack, isFromSalesman);
    }

    /**
     * Wrapper method for updating the message at the DB
     * @param msg The message to be updated
     * @return True on success, otherwise false
     */
    public boolean updateMessage(MessagesEntity msg) {
        return houseDAO.updateMessage(msg);
    }
    // Equals and hashcode --------------------------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OfferService)) return false;

        OfferService that = (OfferService) o;

        if (houseDAO != null ? !houseDAO.equals(that.houseDAO) : that.houseDAO != null) return false;
        if (offerTransactionsDAO != null ? !offerTransactionsDAO.equals(that.offerTransactionsDAO) : that.offerTransactionsDAO != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = offerTransactionsDAO != null ? offerTransactionsDAO.hashCode() : 0;
        result = 31 * result + (houseDAO != null ? houseDAO.hashCode() : 0);
        return result;
    }
}
