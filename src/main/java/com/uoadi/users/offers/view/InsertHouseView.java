package com.uoadi.users.offers.view;

import com.uoadi.users.common.house.entities.location.LocationEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasLessorEntity;
import com.uoadi.users.common.house_transaction.entities.HouseHasSalesmanEntity;
import com.uoadi.users.common.user.entities.UserEntity;
import com.uoadi.users.common.user.service.UserService;
import com.uoadi.users.offers.helper.InsertHouse;
import com.uoadi.users.offers.service.OfferService;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Calendar;

@ViewScoped
@ManagedBean
public class InsertHouseView implements Serializable {
    private static final long serialVersionUID = -8481377211230057402L;
    static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private static final String FLOOR_APT = "Στον ";
    private static final String FLOOR_HOUSE = "Αριθμός ορόφων";
    private static final String FLOOR_APT_END = " όροφο";

    @EJB
    private OfferService houseService;

    private InsertHouse house;
    private Boolean negotiablePrice;
    private String houseType;

    @ManagedProperty(value = "#{userStateBean.current}")
    private UserEntity user;
    @ManagedProperty(value = "#{userStateBean.currentRole}")
    private String currentRole;
    private String floorsLabel;
    private String floorsLabelEnd;
    private int curYear;

    private MapModel emptyModel;

    @PostConstruct
    public void init() {
        house = new InsertHouse(user.getIdUser());
        emptyModel = new DefaultMapModel();
        floorsLabel = FLOOR_APT;
        floorsLabelEnd = FLOOR_APT_END;
        curYear = Calendar.getInstance().get(Calendar.YEAR);
    }

    public void addMarker() {
        double lat = house.getLocationEntity().getLatitude();
        double lng = house.getLocationEntity().getLongitude();
        Marker marker = new Marker(new LatLng(lat, lng), "");
        emptyModel.addOverlay(marker);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Added", "Lat:" + lat + ", Lng:" + lng));
    }

    public String insertHouse() {
        LocationEntity locationEntity = house.getLocationEntity();
        HouseHasSalesmanEntity houseHasSalesmanEntity = house.getHouseHasSalesmanEntity();
        HouseHasLessorEntity houseHasLessorEntity = house.getHouseHasLessorEntity();
        if(locationEntity.getLongitude()==null && locationEntity.getLatitude()==null)
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Πηγαίνετε στη καρτέλα με το χάρτη και τοποθετήστε το σπίτι σας κάνοντας click πανω του", ""));
            return null;
        }
        if(currentRole.equals("salesman")) {
            houseHasLessorEntity = null;
            houseHasSalesmanEntity.setIdUser(user.getIdUser());
        }
        else if(currentRole.equals("lessor")) {
            houseHasLessorEntity.setIdUser(user.getIdUser());
            houseHasSalesmanEntity = null;
        }
        Integer id = houseService.insertHouse(house);
        if(id == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Κάτι δεν πήγε καλά κατά την εισαγωγή του σπιτιού σας, παρακαλώ προσπαθήστε ξανά",""));
            return null;
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Η εισαγωγή του σπιτιού σας έγινε με επιτυχία! Από το προφίλ του σπιτιού μπορείτε να προσθέσετε εικόνες " +
                        "για το σπίτι αλλά και να δείτε\n" +
                        "μηνύματα που σας έχουν στείλει οι ενδιαφερόμενοι χρήστες.", ""));
        return "/offers/landing.xhtml?faces-redirect=true";
    }

    public void houseTypeChanged()  {
        if (houseType.equals("Διαμέρισμα")) {
            floorsLabel = FLOOR_APT;
            floorsLabelEnd = FLOOR_APT_END;
        }
        else if(houseType.equals("Μονοκατοικία")) {
            floorsLabel = FLOOR_HOUSE;
            floorsLabelEnd = "";
        }
    }

    /*Getter-Setter*/

    public InsertHouse getHouse() {
        return house;
    }

    public void setHouse(InsertHouse house) {
        this.house = house;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        if(houseType == null)
            houseType = "";
        this.houseType = houseType;
        Byte value = null;
        if(houseType.equals("Διαμέρισμα")) {
            value = 1;
            house.getHouseEntity().setApartment(value);
        }
        else if(houseType.equals("Μονοκατοικία")) {
            value = 0;
            house.getHouseEntity().setApartment(value);
        }
    }

    public Boolean getNegotiablePrice() {
        return negotiablePrice;
    }

    public void setNegotiablePrice(Boolean negotiablePrice) {
        this.negotiablePrice = negotiablePrice;
        Byte value = 0;
        if(negotiablePrice)
            value = 1;
        house.getValueEntity().setNegotiable(value);
    }

    public int getCurYear() {
        return curYear;
    }

    public void setCurYear(int curYear) {
        this.curYear = curYear;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public MapModel getEmptyModel() {
        return emptyModel;
    }

    public void setEmptyModel(MapModel emptyModel) {
        this.emptyModel = emptyModel;
    }

    public String getFloorsLabelEnd() {
        return floorsLabelEnd;
    }

    public void setFloorsLabelEnd(String floorsLabelEnd) {
        this.floorsLabelEnd = floorsLabelEnd;
    }

    public String getFloorsLabel() {
        return floorsLabel;
    }

    public void setFloorsLabel(String floorsLabel) {
        this.floorsLabel = floorsLabel;
    }

    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InsertHouseView that = (InsertHouseView) o;

        if (curYear != that.curYear) return false;
        if (currentRole != null ? !currentRole.equals(that.currentRole) : that.currentRole != null) return false;
        if (emptyModel != null ? !emptyModel.equals(that.emptyModel) : that.emptyModel != null) return false;
        if (floorsLabel != null ? !floorsLabel.equals(that.floorsLabel) : that.floorsLabel != null) return false;
        if (floorsLabelEnd != null ? !floorsLabelEnd.equals(that.floorsLabelEnd) : that.floorsLabelEnd != null)
            return false;
        if (house != null ? !house.equals(that.house) : that.house != null) return false;
        if (houseService != null ? !houseService.equals(that.houseService) : that.houseService != null) return false;
        if (houseType != null ? !houseType.equals(that.houseType) : that.houseType != null) return false;
        if (negotiablePrice != null ? !negotiablePrice.equals(that.negotiablePrice) : that.negotiablePrice != null)
            return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = houseService != null ? houseService.hashCode() : 0;
        result = 31 * result + (house != null ? house.hashCode() : 0);
        result = 31 * result + (negotiablePrice != null ? negotiablePrice.hashCode() : 0);
        result = 31 * result + (houseType != null ? houseType.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (currentRole != null ? currentRole.hashCode() : 0);
        result = 31 * result + (floorsLabel != null ? floorsLabel.hashCode() : 0);
        result = 31 * result + (floorsLabelEnd != null ? floorsLabelEnd.hashCode() : 0);
        result = 31 * result + curYear;
        result = 31 * result + (emptyModel != null ? emptyModel.hashCode() : 0);
        return result;
    }
}
