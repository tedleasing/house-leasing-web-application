package com.uoadi.users.offers.view;

import com.uoadi.users.common.house.entities.info.ImagesEntity;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.common.house.entities.location.LocationEntity;
import com.uoadi.users.common.user.view.UserStateBean;
import com.uoadi.users.offers.helper.HouseValuePack;
import com.uoadi.users.offers.service.OfferService;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class OffersHouseProfileView implements Serializable{
    static final Logger LOG = LoggerFactory.getLogger(OffersHouseProfileView.class);
    private static final long serialVersionUID = 2996558419834744200L;
    private static final String FLOOR_APT = "Στον ";
    private static final String FLOOR_HOUSE = "Αριθμός ορόφων";
    private static final String FLOOR_APT_END = " όροφο";
    private static final String IMG_BASE = "/users/user";

    @EJB
    private OfferService offerService;

    @ManagedProperty("#{userStateBean}")
    private UserStateBean currentUser;
    private MapModel draggableModel;
    private Marker marker;
    private String center;

    // Data ---------------------------------------------------------------------------------------
    private List<ImagesEntity> images;
    private List<MessagesEntity> messages;
    private ImagesEntity selectedImage;
    private HouseValuePack houseValuePack;
    private HouseValuePack houseValuePackBackup;
    private String houseType;
    private String houseTypeBackup;
    private Boolean negotiablePrice;
    private Boolean negotiablePriceBackup;
    private String floorsLabel;
    private String floorsLabelBackup;
    private String floorsLabelEnd;
    private String floorsLabelEndBackup;
    private String imgPath;
    private String appDirPath;
    private UploadedFile file;
    private MessagesEntity msgToUpdate;

    @PostConstruct
    public void init() {
        houseValuePack = Faces.getSessionAttribute("selectedEditHouse");
        houseValuePackBackup = new HouseValuePack(houseValuePack);
        if (houseValuePack != null) {
            houseType = houseValuePack.getHouse().getApartment() == 1 ? "Διαμέρισμα" : "Μονοκατοικία";
            houseTypeBackup = houseType;
            if (houseValuePack.getHouse().getApartment() == 1) {
                floorsLabel = FLOOR_APT;
                floorsLabelEndBackup = floorsLabelEnd = FLOOR_APT_END;
            }
            else {
                floorsLabel = FLOOR_HOUSE;
                floorsLabelEndBackup = floorsLabelEnd = "";
            }
            floorsLabelBackup = floorsLabel;
            negotiablePrice = houseValuePack.getValue().getNegotiable() == 1;
            negotiablePriceBackup = negotiablePrice;
            // Images handling
            images = new ArrayList<ImagesEntity>();
                for (ImagesEntity img : houseValuePack.getHouse().getImagesByIdHouse())
                    images.add(img);
            // Setting image base path
            appDirPath = System.getProperty("user.home") + "/.harachi";
            imgPath = IMG_BASE + currentUser.getCurrent().getIdUser() + "/house"
                    + houseValuePack.getHouse().getIdHouse() + "/";
            selectedImage = null;
            // Getting messages
            messages = new ArrayList<MessagesEntity>();
                for (MessagesEntity msg : houseValuePack.getHouse().getMessagesByIdHouse())
                    messages.add(msg);
            draggableModel = new DefaultMapModel();
            LocationEntity locationEntity = houseValuePack.getHouse().getLocationByIdLocation();
            Double lat = locationEntity.getLatitude();
            Double lng = locationEntity.getLongitude();
            if(lat != null && lng != null) {
                LatLng latLng = new LatLng(lat, lng);
                draggableModel.addOverlay(new Marker(latLng, houseValuePack.getHouse().getTitle()));
                for (Marker preMarker : draggableModel.getMarkers()) {
                    preMarker.setDraggable(true);
                }
            }
            LOG.info("HouseProfileView: Viewing house [" + houseValuePack.getHouse().getIdHouse() + "]");
        }
    }

    public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();
        LocationEntity locationEntity = houseValuePack.getHouse().getLocationByIdLocation();
        locationEntity.setLatitude(marker.getLatlng().getLat());
        locationEntity.setLongitude(marker.getLatlng().getLng());
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Dragged", "Lat:" + marker.getLatlng().getLat() + ", Lng:" + marker.getLatlng().getLng()));
    }

    public String getHouseTypeDesc() {
        if (houseValuePack.getHouse().getApartment() == 1)
            return "Διαμέρισμα";
        else
            return "Μονοκατοικία";
    }


    public void handleImageUpload(FileUploadEvent event) {
        // Prepare Image Entity
        ImagesEntity imageEntity = new ImagesEntity();
        imageEntity.setHouseByIdHouse(houseValuePack.getHouse());
        imageEntity.setDescription("Πατήστε εδώ για να προσδιορίσετε την περιγραφή");
        imageEntity.setUrl(event.getFile().getFileName());
        // Persist Entity
        boolean successful = offerService.persistImage(imageEntity, true);
        if (successful) {
            // Write to disk
            File uploads = new File(appDirPath + imgPath);
            if (!uploads.exists()) {
                if (!uploads.mkdirs()) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Υπήρξε κάποιο πρόβλημα με το ανέβασμα της εικόνας. Παρακαλώ προσπαθήστε ξανά", "");
                    FacesContext.getCurrentInstance().addMessage("images-message", message);
                    return;
                }
            }
            try {
                File img = new File(uploads, event.getFile().getFileName());
                InputStream input = event.getFile().getInputstream();
                Files.copy(input, img.toPath());
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Η εικόνα αποθηκεύθηκε επιτυχώς!", "");
                FacesContext.getCurrentInstance().addMessage("images-message", message);
                // Add to images list
                images.add(imageEntity);
                // Update house Entity
                houseValuePack.getHouse().getImagesByIdHouse().add(imageEntity);
            } catch (FileAlreadyExistsException e) {
                // Probably shouldn't reach here - DB constraint will catch duplicate image
                offerService.deleteImage(imageEntity);
                images.remove(imageEntity);
                houseValuePack.getHouse().getImagesByIdHouse().remove(imageEntity);
                LOG.error("Image to be uploaded for [" + houseValuePack.getHouse().getIdHouse() + "] already exists: " + e.getMessage());
            } catch (IOException e) {
                offerService.deleteImage(imageEntity);
                LOG.error("Could not upload image for house [" + houseValuePack.getHouse().getIdHouse() + "]: " + e.getMessage());
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Υπήρξε κάποιο πρόβλημα με το ανέβασμα της εικόνας. Παρακαλώ προσπαθήστε ξανά", "");
                FacesContext.getCurrentInstance().addMessage("images-message", message);
                images.remove(imageEntity);
                houseValuePack.getHouse().getImagesByIdHouse().remove(imageEntity);
            }
        }
        else {
            LOG.error("handleImageUpload: Could not persist image for house [" + houseValuePack.getHouse().getIdHouse() + "]");
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Υπήρξε κάποιο πρόβλημα με το ανέβασμα της εικόνας. Παρακαλώ προσπαθήστε ξανά", "");
            FacesContext.getCurrentInstance().addMessage("images-message", message);
        }
    }

    public void saveDescription(ImagesEntity img) {
        // persist image
        boolean successful = offerService.persistImage(img, false);
        // inform user
        if (successful)
            FacesContext.getCurrentInstance().addMessage("images-message", new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Η καινούργια περιγραφή αποθηκεύθηκε επιτυχώς", ""));
        else
            FacesContext.getCurrentInstance().addMessage("images-message", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Υπήρξε κάποιο πρόβλημα με την αποθήκευση της περιγραφής. Παρακαλώ προσπαθήστε ξανά", ""));
    }

    public String isNegotiable() {
        if (negotiablePrice)
            return "Συζητήσιμη";
        else
            return "Μη συζητήσιμη";
    }

    public void houseTypeChanged()  {
        if (houseType.equals("Διαμέρισμα")) {
            floorsLabel = FLOOR_APT;
            floorsLabelEnd = FLOOR_APT_END;
        }
        else {
            floorsLabel = FLOOR_HOUSE;
            floorsLabelEnd = "";
        }
    }


    /**
     * Deletes the image from the db,
     * and the disk, updates the local
     * list of Images and also the
     * HouseValuePack object
     */
    public void deleteImage() {
        if (selectedImage != null) {
            // Delete image from the db
            boolean successful = offerService.deleteImage(selectedImage);
            if (!successful)
                FacesContext.getCurrentInstance().addMessage("images-message",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Υπήρξε κάποιο σφάλμα με την διαγραφή " +
                                "την εικόνας, παρακαλώ προσπαθήστε ξανά", ""));
            else {
                // Remove from disk
                File imgFile = new File(appDirPath + imgPath + selectedImage.getUrl());
                boolean deletedSuccessfully = imgFile.delete();
                if (deletedSuccessfully) {
                    // If house directory is empty, delete it
                    File houseDir = new File(appDirPath + imgPath);
                    if (houseDir.list().length == 0)
                        houseDir.delete();
                    File userDir = new File(appDirPath + "/users/user" + currentUser.getCurrent().getIdUser());
                    if (userDir.list().length == 0)
                        userDir.delete();
                    // Delete from local list
                    images.remove(selectedImage);
                    // Delete from the collection of the HouseValuePack
                    houseValuePack.getHouse().getImagesByIdHouse().remove(selectedImage);
                    selectedImage = null;
                    FacesContext.getCurrentInstance().addMessage("images-message",
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Η εικόνα διαγράφηκε με επιτυχία", ""));
                }
                else
                    FacesContext.getCurrentInstance().addMessage("images-message",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Υπήρξε κάποιο σφάλμα με την διαγραφή " +
                                    "την εικόνας, παρακαλώ προσπαθήστε ξανά", ""));
            }
        }
    }

    public boolean deleteAllImages() {
        // Remove all images from disk
        while (houseValuePack.getHouse().getImagesByIdHouse().iterator().hasNext()) {
            ImagesEntity imgToDelete = houseValuePack.getHouse().getImagesByIdHouse().iterator().next();
            File imgFile = new File(appDirPath + imgPath + imgToDelete.getUrl());
            boolean deletedSuccessfully = imgFile.delete();
            if (deletedSuccessfully) {
                // If house directory is empty, delete it
                File houseDir = new File(appDirPath + imgPath);
                if (houseDir.list().length == 0)
                    houseDir.delete();
                File userDir = new File(appDirPath + "/users/user" + currentUser.getCurrent().getIdUser());
                if (userDir.list().length == 0)
                    userDir.delete();
                return true;
            } else
                return false;
        }
        // No images, all good
        return true;
    }

    public void updateHouse() {
        Integer negotiablePriceInt = negotiablePrice ? 1:0;
        houseValuePack.getValue().setNegotiable(negotiablePriceInt.byteValue());
        Integer apartment = houseType.contains("Διαμέρισμα") ? 1:0;
        houseValuePack.getHouse().setApartment(apartment.byteValue());
        // persist House and Value
        String outcome = offerService.updateHouseProfile(houseValuePack);
        if (outcome.equals(OfferService.RESULT_HV_NONE_UPDATED))
            FacesContext.getCurrentInstance().addMessage("house-info", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    outcome, ""));
        else if (outcome.equals(OfferService.RESULT_HV_HOUSE_UPDATED))
            FacesContext.getCurrentInstance().addMessage("house-info", new FacesMessage(FacesMessage.SEVERITY_WARN,
                    outcome, ""));
        else if (outcome.equals(OfferService.RESULT_HV_VALUE_UPDATED))
            FacesContext.getCurrentInstance().addMessage("house-info", new FacesMessage(FacesMessage.SEVERITY_WARN,
                    outcome, ""));
        else {
            FacesContext.getCurrentInstance().addMessage("house-info", new FacesMessage(FacesMessage.SEVERITY_INFO,
                    outcome, ""));
            // update backup
            houseValuePackBackup = new HouseValuePack(houseValuePack);
        }
        // update labels
        floorsLabelBackup = floorsLabel;
        floorsLabelEndBackup = floorsLabelEnd;
    }

    public void cancelChanges() {
        houseValuePack = new HouseValuePack(houseValuePackBackup);
        houseType = houseTypeBackup;
        negotiablePrice = negotiablePriceBackup;
        floorsLabel = floorsLabelBackup;
        floorsLabelEnd = floorsLabelEndBackup;
        FacesContext.getCurrentInstance().addMessage("house-info", new FacesMessage(FacesMessage.SEVERITY_WARN,
                "Οι αλλαγές σας ακυρώθηκαν επιτυχώς", ""));
    }

    public String deleteHouse() {
        boolean isFromSalesman = currentUser.getCurrentRole().equals(UserStateBean.SALESMAN);
        boolean wasSuccessful = offerService.deleteHouse(houseValuePack, isFromSalesman);
        if (wasSuccessful) {
            // Delete images at the filesystem
            deleteAllImages();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Η οικία διαγράφηκε επιτυχώς", "");
            Faces.setFlashAttribute("deleted-house-msg", msg);
            return "/offers/landing?faces-redirect=true";
        }
        else {
            FacesContext.getCurrentInstance().addMessage("delete-house-messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Υπήρξε κάποιο πρόβλημα με τη διαγραφή της οικίας σας, παρακαλώ προσπαθήστε ξανά",
                    "Εάν το πρόβλημα επιμείνει επικοινωνήστε με τον διαχειριστή"));
            return null;
        }
    }

    public void setReadMessage() {
        Integer isRead = 1;
        msgToUpdate.setIsRead(isRead.byteValue());
        offerService.updateMessage(msgToUpdate);
    }
    // Getters and setters, equals and hashcode ---------------------------------------------------
    public MessagesEntity getMsgToUpdate() {
        return msgToUpdate;
    }

    public void setMsgToUpdate(MessagesEntity msgToUpdate) {
        this.msgToUpdate = msgToUpdate;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<MessagesEntity> getMessages() {
        return messages;
    }

    public void setMessages(List<MessagesEntity> messages) {
        this.messages = messages;
    }

    public ImagesEntity getSelectedImage() {
        return selectedImage;
    }

    public void setSelectedImage(ImagesEntity selectedImage) {
        this.selectedImage = selectedImage;
    }

    public UserStateBean getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserStateBean currentUser) {
        this.currentUser = currentUser;
    }

    public String getImgPath() {
        return imgPath;
    }

    public List<ImagesEntity> getImages() {
        return images;
    }

    public void setImages(List<ImagesEntity> images) {
        this.images = images;
    }

    public String getFloorsLabelEnd() {
        return floorsLabelEnd;
    }

    public void setFloorsLabelEnd(String floorsLabelEnd) {
        this.floorsLabelEnd = floorsLabelEnd;
    }

    public String getFloorsLabel() {
        return floorsLabel;
    }

    public void setFloorsLabel(String floorsLabel) {
        this.floorsLabel = floorsLabel;
    }

    public Boolean getNegotiablePrice() {
        return negotiablePrice;
    }

    public void setNegotiablePrice(Boolean negotiablePrice) {
        this.negotiablePrice = negotiablePrice;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public HouseValuePack getHouseValuePack() {
        return houseValuePack;
    }

    public void setHouseValuePack(HouseValuePack houseValuePack) {
        this.houseValuePack = houseValuePack;
    }

    public String getCenter() {
        LocationEntity locationEntity = houseValuePack.getHouse().getLocationByIdLocation();
        Double lat = locationEntity.getLatitude();
        Double lng = locationEntity.getLongitude();
        String res="37.6779298,25.6647722,720316";
        if(lat != null && lng != null)
            res =  "" + lat + "," + lng;
        return res;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public MapModel getDraggableModel() {
        return draggableModel;
    }

    public void setDraggableModel(MapModel draggableModel) {
        this.draggableModel = draggableModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OffersHouseProfileView)) return false;

        OffersHouseProfileView that = (OffersHouseProfileView) o;

        if (floorsLabel != null ? !floorsLabel.equals(that.floorsLabel) : that.floorsLabel != null) return false;
        if (floorsLabelBackup != null ? !floorsLabelBackup.equals(that.floorsLabelBackup) : that.floorsLabelBackup != null)
            return false;
        if (floorsLabelEnd != null ? !floorsLabelEnd.equals(that.floorsLabelEnd) : that.floorsLabelEnd != null)
            return false;
        if (floorsLabelEndBackup != null ? !floorsLabelEndBackup.equals(that.floorsLabelEndBackup) : that.floorsLabelEndBackup != null)
            return false;
        if (houseType != null ? !houseType.equals(that.houseType) : that.houseType != null) return false;
        if (houseTypeBackup != null ? !houseTypeBackup.equals(that.houseTypeBackup) : that.houseTypeBackup != null)
            return false;
        if (houseValuePack != null ? !houseValuePack.equals(that.houseValuePack) : that.houseValuePack != null)
            return false;
        if (houseValuePackBackup != null ? !houseValuePackBackup.equals(that.houseValuePackBackup) : that.houseValuePackBackup != null)
            return false;
        if (images != null ? !images.equals(that.images) : that.images != null) return false;
        if (negotiablePrice != null ? !negotiablePrice.equals(that.negotiablePrice) : that.negotiablePrice != null)
            return false;
        if (negotiablePriceBackup != null ? !negotiablePriceBackup.equals(that.negotiablePriceBackup) : that.negotiablePriceBackup != null)
            return false;
        if (!offerService.equals(that.offerService)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = offerService.hashCode();
        result = 31 * result + (images != null ? images.hashCode() : 0);
        result = 31 * result + (houseValuePack != null ? houseValuePack.hashCode() : 0);
        result = 31 * result + (houseValuePackBackup != null ? houseValuePackBackup.hashCode() : 0);
        result = 31 * result + (houseType != null ? houseType.hashCode() : 0);
        result = 31 * result + (houseTypeBackup != null ? houseTypeBackup.hashCode() : 0);
        result = 31 * result + (negotiablePrice != null ? negotiablePrice.hashCode() : 0);
        result = 31 * result + (negotiablePriceBackup != null ? negotiablePriceBackup.hashCode() : 0);
        result = 31 * result + (floorsLabel != null ? floorsLabel.hashCode() : 0);
        result = 31 * result + (floorsLabelBackup != null ? floorsLabelBackup.hashCode() : 0);
        result = 31 * result + (floorsLabelEnd != null ? floorsLabelEnd.hashCode() : 0);
        result = 31 * result + (floorsLabelEndBackup != null ? floorsLabelEndBackup.hashCode() : 0);
        return result;
    }
}
