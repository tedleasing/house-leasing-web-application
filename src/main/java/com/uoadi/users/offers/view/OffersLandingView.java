package com.uoadi.users.offers.view;


import com.uoadi.users.common.house.entities.HouseEntity;
import com.uoadi.users.common.house.entities.info.MessagesEntity;
import com.uoadi.users.offers.helper.HouseValuePack;
import com.uoadi.users.offers.service.OfferService;
import com.uoadi.users.common.user.service.UserService;
import com.uoadi.users.common.user.view.UserStateBean;
import org.omnifaces.util.Faces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@ManagedBean
@ViewScoped
public class OffersLandingView implements Serializable {
    static final Logger LOG = LoggerFactory.getLogger(OffersLandingView.class);
    private static final long serialVersionUID = -3958577849163030825L;
    // Properties ---------------------------------------------------------------------------------
    @ManagedProperty("#{userStateBean}")
    private UserStateBean userStateBean;

    @EJB
    private UserService userService; // PROBABLY NOT NEEDED
    @EJB
    private OfferService offerService;

    // Data ---------------------------------------------------------------------------------------
    private List<HouseValuePack> housesValues;
    private HouseValuePack selectedHouse;
    private int number = 10;
    private String imgPath;
    private HashMap<Integer, Integer> unReadMessages;

    @PostConstruct
    public void init() {
        boolean isSalesman = userStateBean.getCurrentRole().equals(UserStateBean.SALESMAN);
        housesValues = offerService.getOfferedHouses(userStateBean.getCurrent().getIdUser(),
                isSalesman);
        if (housesValues != null) {
            number = housesValues.size();
            // Initialize unread messages-counter map
            unReadMessages = new HashMap<Integer, Integer>();
            for(HouseValuePack hvp : housesValues) {
                int unRead = countUnRead(hvp.getHouse().getMessagesByIdHouse());
                unReadMessages.put(hvp.getHouse().getIdHouse(), unRead);
            }
            LOG.info(userStateBean.getCurrentRole() + " [" + userStateBean.getCurrent().getUsername() + "] reviewed" +
                        " his/her houses");
        }
    }

    /**
     * Counts the unread messages of the collection
     * @param messages The collection of messages whose
     *                 unread number will be summed
     * @return The number of unread messages
     */
    private int countUnRead(Collection<MessagesEntity> messages) {
        int unreadCount = 0;
        for (MessagesEntity msg : messages){
            if (msg.getIsRead() == 0)
                unreadCount++;
        }
        return unreadCount;
    }

    public String viewThisHouse() {
        if (selectedHouse != null) {
            Faces.setSessionAttribute("selectedEditHouse", selectedHouse);
            return "/offers/house.xhtml" + "?faces-redirect=true";
        }
        else
            return null;
    }

    public void getMessages(ComponentSystemEvent event) {
//        FacesMessage msg = (FacesMessage) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deleted-house-msg");
        FacesMessage msg = Faces.getFlashAttribute("deleted-house-msg");
        if (msg != null) {
            FacesContext.getCurrentInstance().addMessage("inform-messages", msg);
        }
    }

    /**
     * Checks whether the owner has unread messages for this house
     * (Used in an EL rendered expression)
     * @param house The house to be checked for unread messages
     * @return The number of unread messages
     */
    public Integer getUnreadMessagesCount(HouseEntity house) {
        if (unReadMessages.containsKey(house.getIdHouse()))
            return unReadMessages.get(house.getIdHouse());
        else
            return 0;
    }

    // Getters and setters, equals and hashcode ---------------------------------------------------
    public HashMap<Integer, Integer> getUnReadMessages() {
        return unReadMessages;
    }

    public void setUnReadMessages(HashMap<Integer, Integer> unReadMessages) {
        this.unReadMessages = unReadMessages;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<HouseValuePack> getHousesValues() {
        return housesValues;
    }

    public void setHousesValues(List<HouseValuePack> housesValues) {
        this.housesValues = housesValues;
    }

    public HouseValuePack getSelectedHouse() {
        return selectedHouse;
    }

    public void setSelectedHouse(HouseValuePack selectedHouse) {
        this.selectedHouse = selectedHouse;
    }

    public UserStateBean getUserStateBean() {
        return userStateBean;
    }

    public void setUserStateBean(UserStateBean userStateBean) {
        this.userStateBean = userStateBean;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OffersLandingView)) return false;

        OffersLandingView that = (OffersLandingView) o;

        if (housesValues != null ? !housesValues.equals(that.housesValues) : that.housesValues != null) return false;
        if (selectedHouse != null ? !selectedHouse.equals(that.selectedHouse) : that.selectedHouse != null)
            return false;
        if (!userService.equals(that.userService)) return false;
        if (userStateBean != null ? !userStateBean.equals(that.userStateBean) : that.userStateBean != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userStateBean != null ? userStateBean.hashCode() : 0;
        result = 31 * result + userService.hashCode();
        result = 31 * result + (housesValues != null ? housesValues.hashCode() : 0);
        result = 31 * result + (selectedHouse != null ? selectedHouse.hashCode() : 0);
        return result;
    }
}
