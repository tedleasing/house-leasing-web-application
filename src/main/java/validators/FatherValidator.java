package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



@FacesValidator("validators.FatherValidator")
public class FatherValidator implements Validator{
    private static final String FATHER_PATTERN = "^[A-Za-zΑ-Ωα-ωίϊΐόάέύϋΰήώ]{0,45}$";

    private Pattern pattern;
    private Matcher matcher;

    public FatherValidator(){
        pattern = Pattern.compile(FATHER_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Μη αποδεκτό πατρώνυμο. Το πατρώνυμο σας πρεπεί να αποτελείται από 3-45 " +
                    "πεζόυς ή κεφαλαίους ελλήνικους ή λατινικούς χαρακτήρες",
                    "Το πατρώνυμο που πληκτρολογήσατε δεν είναι αποδεκτό");
            throw new ValidatorException(message);
        }
//        }
    }
}
