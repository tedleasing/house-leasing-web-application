package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



@FacesValidator("validators.MobileValidator")
public class MobileValidator implements Validator{
    private static final String MOBILE_PATTERN = "^(((\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}){0,1})|(___-___-____)$";

    private Pattern pattern;
    private Matcher matcher;

    public MobileValidator(){
        pattern = Pattern.compile(MOBILE_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Μη αποδεκτό κινητό τηλέφωνο. Το κινητό σας πρεπεί να αποτελείται από" +
                    " 10 ψηφία ",
                    "Το κινητό που πληκτρολογήσατε δεν είναι αποδεκτό");
            throw new ValidatorException(message);
        }
//        }
    }
}
