package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@FacesValidator("validators.MoneyValidator")
public class MoneyValidator implements Validator {
    private static final String MONEY_PATTERN = "(^\\d{1,8})$";

    private Pattern pattern;
    private Matcher matcher;

    public MoneyValidator(){
        pattern = Pattern.compile(MONEY_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Μη αποδεκτός τύπος χρημάτων",
                    "Το ποσό που πληκτρολογήσατε δεν είναι αποδεκτό");
            throw new ValidatorException(message);
        }
    }
}
