package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



@FacesValidator("validators.NameValidator")
public class NameValidator implements Validator{
    private static final String NAME_PATTERN = "^[A-Za-zΑ-Ωα-ωίϊΐόάέύϋΰήώ]{0,45}$";

    private Pattern pattern;
    private Matcher matcher;

    public NameValidator(){
        pattern = Pattern.compile(NAME_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Μη αποδεκτό όνομα. Το όνομα σας πρεπεί να αποτελείται από 3-45 " +
                    "πεζόυς ή κεφαλαίους ελλήνικους ή λατινικούς χαρακτήρες",
                    "Το όνομα που πληκτρολογήσατε δεν είναι αποδεκτό");
            throw new ValidatorException(message);
        }
    }
}
