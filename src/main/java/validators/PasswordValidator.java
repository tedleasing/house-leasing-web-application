package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



@FacesValidator("validators.PasswordValidator")
public class PasswordValidator implements Validator{
    private static final String PASSWORD_PATTERN = "^[A-ZA-zΑ-Ωα-ωίϊΐόάέύϋΰήώ0-9_-]{4,45}$";

    private Pattern pattern;
    private Matcher matcher;

    public PasswordValidator(){
        pattern = Pattern.compile(PASSWORD_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Ο κωδικός σας δεν είναι αποδεκτός. Ο κωδικός πρέπει να αποτελείται από" +
                    " 4-45 ελληνικούς ή λατινικούς χαρακτήρες ή αριθμόυς ή '_' ή '-'",
                    "Η μορφή του κωδικού δεν είναι αποδεκτή");
            throw new ValidatorException(message);
        }
    }
}


