package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



@FacesValidator("validators.SurnameValidator")
public class SurnameValidator implements Validator{
    private static final String SURNAME_PATTERN = "^[A-Za-zΑ-Ωα-ωίϊΐόάέύϋΰήώ]{0,45}$";

    private Pattern pattern;
    private Matcher matcher;

    public SurnameValidator(){
        pattern = Pattern.compile(SURNAME_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Μη αποδεκτό επίθετο. Το επίθετο σας πρεπεί να αποτελείται από 3-45 " +
                    "πεζόυς ή κεφαλαίους ελλήνικους ή λατινικούς χαρακτήρες",
                    "Το επίθετο που πληκτρολογήσατε δεν είναι αποδεκτό");
            throw new ValidatorException(message);
        }
//        }
    }
}
