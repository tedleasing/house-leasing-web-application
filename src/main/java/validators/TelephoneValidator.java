package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



@FacesValidator("validators.TelephoneValidator")
public class TelephoneValidator implements Validator{
    private static final String TELEPHONE_PATTERN = "^(((\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}){0,1})|(___-___-____)$";

    private Pattern pattern;
    private Matcher matcher;

    public TelephoneValidator(){
        pattern = Pattern.compile(TELEPHONE_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Μη αποδεκτό σταθερό τηλέφωνο. Το τηλέφωνο σας πρεπεί να αποτελείται από" +
                    " 10 ψηφία ",
                    "Το σταθερό που πληκτρολογήσατε δεν είναι αποδεκτό");
            throw new ValidatorException(message);
        }
//        }
    }
}
