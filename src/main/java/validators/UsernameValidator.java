package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@FacesValidator("validators.UsernameValidator")
public class UsernameValidator implements Validator{
    private static final String USERNAME_PATTERN = "^[A-ZA-zΑ-Ωα-ωίϊΐόάέύϋΰήώ0-9_-]{3,45}$";

    private Pattern pattern;
    private Matcher matcher;

    public UsernameValidator(){
        pattern = Pattern.compile(USERNAME_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        if(value == null)
            return;

        matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {

            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Το όνομα χρήστη πρεπει να αποτελείται από 3-45 χαρακτήρες ελληνικούς ή " +
                            "λατινικούς ή αριθμόυς ή '_' ή '-'",
                    "Μη αποδεκτό όνομα χρήστη");
            throw new ValidatorException(message);
        }
    }
}
