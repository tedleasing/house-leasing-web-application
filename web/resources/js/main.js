
$( document ).ready(function() {
    // Adding a nice glyphicon to the ugly Primeface error message
    $('.ui-messages-error-icon').addClass("remove-ugly-error-icon fa fa-warning");

    $('#search-btn').click(function () {
        $('html, body').animate({
            scrollTop: $("#housesValues").offset().top,
            easing: 'swing'
        }, 1400);
    })
});

